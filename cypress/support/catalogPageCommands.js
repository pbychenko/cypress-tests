import catalogPageElements from'../pageElements/catalogPageElements';

// Cypress.Commands.add("getProductPriceByNumber", (number) => {
//     cy.get(`.CategoryPageList-Item:nth-child(${number}) .ProductCardNext__PricesWrapper p:nth-child(1)`).invoke('text')
// })

// Cypress.Commands.add("addProductToWishListByNumber", (number) => {
//     cy.get(`.CategoryProductList-Page >li:nth-child(${number}) .ProductWishlistButton-Button`).click( {force: true} )
// })

// Cypress.Commands.add("openProductPageByNumber", (number) => {
//     cy.get(`.CategoryPageList-Item:nth-child(${number}) a`).click( {force: true} )
// })

const getAlias = (categoryName) => {
  const api = `${Cypress.env('catalogApi')}/products/category/${categoryName}*`
  cy.intercept(api).as('getProducts')
} 

Cypress.Commands.add("fillPriceFilters", (minPrice, maxPrice, categoryName) => {
  getAlias(categoryName)    
  
  cy.get('#priceFilterFrom').clear().type(`${minPrice}{enter}`)
  cy.wait('@getProducts').its('response.statusCode').should('eq', 200)
  cy.url().should('include', `priceMin=${minPrice}`)

  cy.get('#priceFilterTo').clear().type(`${maxPrice}{enter}`)
  cy.wait('@getProducts').its('response.statusCode').should('eq', 200)
  cy.url().should('include', `priceMax=${maxPrice}`)
})

Cypress.Commands.add("selectBrandInFilters", (brand, categoryUrl) => {
  cy.get(`[href="${categoryUrl}/f/brands/${brand.toLowerCase()}"]`).click()
  cy.url().should('include', `f/brands/${brand.toLowerCase()}`)
})

Cypress.Commands.add("selectSorting", (sortingType, categoryName) => {
  const map = {
    'asc': catalogPageElements.categorySortByAscSelector,
    'desc': catalogPageElements.categorySortByDescSelector
  }

  getAlias(categoryName)

  cy.get(catalogPageElements.categorySortSelector).click()
  cy.get(map[sortingType]).click()
  
  cy.wait('@getProducts').its('response.statusCode').should('eq', 200)
})
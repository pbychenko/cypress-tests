const fillAddressFields = (addressData) => {
  const {
    city,
    shortAddress,
    floorNumber,
    flatNumber
  } = addressData
   
  cy.get('#Receivers-apartment').should('be.visible').should('be.enabled').clear().type(flatNumber, {force: true}).should('have.value', flatNumber)
  cy.get('#Receivers-floor').click({force: true}).clear().type(floorNumber, {force: true}).should('have.value', floorNumber+' ')
  cy.get('#Receivers__CityInput').clear().type(city,{force: true}).should('have.value', city)
  // cy.get('#react-autowhatever-1--item-0').click({force: true})
  
  cy.get('#ShippingStepMainAddressInput').click({force: true}).clear().type(shortAddress, {force: true}).should('have.value', shortAddress)
}

const fillMainReceiverFields = (mainReceiverFields) => {
  cy.get('#Receivers-firstName').clear().type(mainReceiverFields.firstName).should('have.value', mainReceiverFields.firstName)
  cy.get('#Receivers-lastName').clear().type(mainReceiverFields.lastName).should('have.value', mainReceiverFields.lastName)
  cy.get('#Receivers-phone').clear().type(mainReceiverFields.phone, { force: true })
  cy.get('#Receivers-email').clear().type(mainReceiverFields.email).should('have.value', mainReceiverFields.email)
}

const fillJurFields = (jurData) => {
  const { orgName, orgBIN, orgBIK, orgAddress, orgIIC } = jurData
  cy.get('#Receivers-orgName').type(orgName, { force: true }).should('have.value', orgName)
  cy.get('#Receivers-orgBIN').type(orgBIN, { force: true }).should('have.value', orgBIN)
  cy.get('#Receivers-orgBIK').type(orgBIK, { force: true }).should('have.value', orgBIK)
  cy.get('#Receivers-orgAddress').type(orgAddress, { force: true }).should('have.value', orgAddress)
  cy.get('#Receivers-orgIIC').type(orgIIC, { force: true }).should('have.value', orgIIC)
}

Cypress.Commands.add('addAddress', (addressData) => {
  cy.get('.receivers-and-addresses .address-details__add-button').contains('Добавить новый адрес').click()
  cy.contains('Добавление адреса')
  fillAddressFields(addressData)
  cy.get('.receivers-and-addresses__form-buttons .ButtonNext').contains('Добавить').click()
  cy.get('.receivers-and-addresses .address-details__add-button').contains('Добавить новый адрес').should('be.visible')
})

Cypress.Commands.add('editAddress', (addressData) => {
  cy.get('.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__change-button').contains('Изменить').click()
  cy.contains('Изменение адреса')
  fillAddressFields(addressData)
  cy.get('.receivers-and-addresses__form-buttons .ButtonNext').contains('Сохранить').click()
  cy.get('.receivers-and-addresses .address-details__add-button').contains('Добавить новый адрес').should('be.visible')
})

Cypress.Commands.add('addReceiver', (receiverData, isPhys = true) => {
  cy.get('.receivers-and-addresses__tabs .Tabs__Tab:last-child').contains('Получатели').click()
  cy.get('.receivers-and-addresses .receiver-details__add-button').contains('Добавить нового получателя').click()
  cy.contains('Добавление получателя')
  fillMainReceiverFields(receiverData)

  // if (!isPhys) {
  //   cy.get('#Receivers__IsOrganization').click()
  //   fillJurFields(receiverData.jurData)
  // }
  cy.get('.receivers-and-addresses__form-buttons .ButtonNext').contains('Добавить').click()
  cy.get('.receivers-and-addresses .receiver-details__add-button').contains('Добавить нового получателя').should('be.visible')
})

Cypress.Commands.add('editReceiver', (receiverData, isPhys = true) => {
  cy.get('.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__change-button').contains('Изменить').click()
  cy.contains('Изменение получателя')
  fillMainReceiverFields(receiverData)

  // if (!isPhys) {
  //   cy.get('#Receivers__IsOrganization').click()
  //   fillJurFields(receiverData.jurData)
  // }

  cy.get('.receivers-and-addresses__form-buttons .ButtonNext').contains('Изменить').click()
  cy.get('.receivers-and-addresses .receiver-details__add-button').contains('Добавить нового получателя').should('be.visible')
})

Cypress.Commands.add('moveReceiverToJurType', (jurData) => {
  cy.get('.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__change-button').contains('Изменить').click()
  cy.contains('Изменение получателя')

  cy.get('[for="Receivers__IsOrganization"] span').click()
  fillJurFields(jurData)

  cy.get('.receivers-and-addresses__form-buttons .ButtonNext').contains('Изменить').click()
  cy.get('.receivers-and-addresses .receiver-details__add-button').contains('Добавить нового получателя').should('be.visible')
})

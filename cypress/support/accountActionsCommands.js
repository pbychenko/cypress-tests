const deleteReceiverById = (id, authorization) => {
  const deleteOptions = {
    method: 'Delete',
    url: `${Cypress.env('ssoUrlV2')}/profile/receivers/${id}`,
    headers: {
      authorization,
    }
  };
  cy.request(deleteOptions).its('status').should('eq', 204)
}

Cypress.Commands.add('clearReceivers', () => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`

  const options = {
    method: 'GET',
    url: `${Cypress.env('ssoUrlV2')}/profile/receivers`,
    headers: {
      authorization,
    }
  }

  cy.request(options).then(({ body }) => {
    expect(body).to.have.property('receivers')
    const ids = body.receivers.map(el => el.id)
    ids.forEach((id) => {
      deleteReceiverById(id, authorization)
    })
  })
})

const deleteAddressById = (id, authorization) => {
  const deleteOptions = {
    method: 'Delete',
    url: `${Cypress.env('ssoUrlV2')}/profile/addresses/${id}`,
    headers: {
      authorization,
    }
  };
  cy.request(deleteOptions).its('status').should('eq', 204)

}

Cypress.Commands.add('clearAddresses', () => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`
  const options = {
    method: 'GET',
    url: `${Cypress.env('ssoUrlV2')}/profile/addresses`,
    headers: {
      authorization,
    }
  }

  cy.request(options).then(({ body }) => {
    expect(body).to.have.property('addresses')
    const ids = body.addresses.map(el => el.id)
    if (ids.length !== 0) {
      ids.forEach((id) => {
        deleteAddressById(id, authorization)
      })
    }
  })
})

Cypress.Commands.add('addReceiverToAccountViaApi', (receiver) => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`

  const postOptions = {
    method: 'Post',
    url: `${Cypress.env('ssoUrlV2')}/profile/receivers`,
    headers: {
      authorization,
    },
    body: receiver
  }

  cy.request(postOptions).then((resp) => {
    expect(resp.status).to.eq(200)
  })
})

Cypress.Commands.add('addAddresToAccountViaApi', (address) => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`

  const postOptions = {
    method: 'Post',
    url: `${Cypress.env('ssoUrlV2')}/profile/addresses`,
    headers: {
      authorization,
    },
    body: address
  }

  cy.request(postOptions).then((resp) => {
    expect(resp.status).to.eq(200)
  })
})


Cypress.Commands.add('getProfileTdid', () => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`

  const getOptions = {
    method: 'Get',
    url: `${Cypress.env('ssoUrl')}/profile`,
    headers: {
      authorization,
    }
  }
  cy.request(getOptions).then((resp) => {
    Cypress.env(resp.tdid, 'tdid')
    expect(resp.status).to.eq(200)
  })
})

Cypress.Commands.add('updateProfile', (userData) => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`

  const putOptions = {
    method: 'Put',
    url: `${Cypress.env('ssoUrl')}/profile`,
    headers: {
      authorization,
    },
    body: userData
  }

  cy.request(putOptions).then((resp) => {
    expect(resp.status).to.eq(200)
  })
})


Cypress.Commands.add('changePasswordViaApi', (oldPassword, newPassword) => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`

  const putOptions = {
    method: 'Put',
    url: `${Cypress.env('ssoUrl')}/profile/password`,
    headers: {
      authorization,
    },
    body: {
      'old_password': oldPassword,
      'password': newPassword
    }
  }

  cy.request(putOptions).then((resp) => {
    expect(resp.status).to.eq(200)
  })
})
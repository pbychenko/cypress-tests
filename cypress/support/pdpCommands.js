import productPageElements from '../pageElements/productPageElements'
import commonElements from '../pageElements/commonElements'

// Cypress.Commands.add("addProductToWishList", () => {
//     cy.get('.ProductWishlistButton-Button span').click({force: true})
// })

Cypress.Commands.add("addProductToCompare", (sku) => {
  cy.visit(`/p/${sku}`)
  cy.get(productPageElements.addToCompareButton).should('be.visible').should('contain', 'Сравнить').click()
  cy.get(commonElements.notification).should('contain.text', 'Товар добавлен в список сравнения')
})
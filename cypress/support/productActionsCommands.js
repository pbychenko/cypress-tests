
Cypress.Commands.add('addToGuestCartViaApi', (sku) => {
  let count = 0
  while (count < 5) {
    if (localStorage.getItem('checkout') !== null) {
      break
    }
    cy.wait(1000)
    count += 1
    console.log(count)
  }

  const cartId = JSON.parse(localStorage.getItem('checkout')).cartId;
  const city_id = '5f5f1e3b4c8a49e692fefd70'
  const postOptions = {
    method: 'Post',
    url: `${Cypress.env('cartApiV2')}/carts/${cartId}/products`,
    body: {
      product: { sku, type: "regular" }
    },
    qs: {
      city_id
    },
  }

  cy.request(postOptions).then((resp) => {
    expect(resp.body).to.have.property('new_products_count', 1)
    expect(resp.status).to.eq(200)
  })
})

Cypress.Commands.add('addToUserCartViaApi', (sku) => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`
  const city_id = '5f5f1e3b4c8a49e692fefd70'
  const postOptions = {
    method: 'Post',
    url: `${Cypress.env('cartApiV2')}/carts/products`,
    headers: {
      authorization,
    },
    body: {
      product: { sku, type: "regular" }
    },
    qs: {
      city_id
    },
  }

  cy.request(postOptions).then((resp) => {
    expect(resp.body).to.have.property('new_products_count', 1)
    expect(resp.status).to.eq(200)
  })
})

Cypress.Commands.add('addServiceToUserCartViaApi', (sku) => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`
  const city_id = '5f5f1e3b4c8a49e692fefd70'
  const postOptions = {
    method: 'Post',
    url: `${Cypress.env('cartApiV2')}/carts/products`,
    headers: {
      authorization,
    },
    body: {
      product: { sku, type: "service" }
    },
    qs: {
      city_id
    },
  }

  cy.request(postOptions).then((resp) => {
    expect(resp.body).to.have.property('new_products_count', 1)
    expect(resp.status).to.eq(200)
  })
})

Cypress.Commands.add('clearUserCartViaApi', () => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`
  const city_id = '5f5f1e3b4c8a49e692fefd70'
  const deleteOptions = {
    method: 'Delete',
    url: `${Cypress.env('cartApiV2')}/carts/products`,
    headers: {
      authorization,
    },
    qs: {
      city_id
    },
  }

  cy.request(deleteOptions).then((resp) => {
    expect(resp.body).to.have.property('product_count', 0)
    expect(resp.status).to.eq(200)
  })
})

Cypress.Commands.add('addToUserWishlistViaApi', (sku) => {
  const token = Cypress.env('token')
  const authorization = `Bearer ${token}`
  const postOptions = {
    method: 'Post',
    url: `${Cypress.env('wishlistApi')}/products`,
    headers: {
      authorization,
    },
    body: {
      sku
    }
  }

  cy.request(postOptions).then((resp) => {
    expect(resp.status).to.eq(200)
  })
})

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('uiLogin', (phone, password = '') => {
  cy.get('.--account').click()
  cy.get('.sign-in-form').should('be.visible')
  cy.get('#sign-in-form__password').should('not.exist')

  cy.get('#sign-in-form__phone').clear().type(phone)
  cy.get('.sign-in-form__buttons').click()

  cy.get('#sign-in-form__password').clear().type(password)
  cy.get('.sign-in-form__buttons').click()
})


Cypress.Commands.add('requestLogin', (existedUser) => {
  cy.request('POST', `${Cypress.env('ssoUrl')}/auth/signin/phone`, existedUser).then(
    ({ body }) => {
      cy.setCookie('accessToken', body.access_token)
      expect(body).to.have.property('status', 'SignIn success')
      Cypress.env('token', body.access_token);
    })
})

// Cypress.Commands.add(
//   'uiRegister',
//   (firstname, lastname, email, phone, password = '') => {
//     cy.get('.Header-AuthBtn_type_checkin').click()
//     if (firstname !== '') {
//       cy.get('#firstname').clear().type(firstname)
//     } else {
//       cy.get('#firstname').invoke('val', '')
//     }

//     if (lastname !== '') {
//       cy.get('#lastname').clear().type(lastname)
//     } else {
//       cy.get('#lastname').invoke('val', '')
//     }

//     if (lastname !== '') {
//       cy.get('#phone').clear().type(phone)
//     } else {
//       cy.get('#phone').invoke('val', '')
//     }

//     if (lastname !== '') {
//       cy.get('#email').clear().type(email)
//     } else {
//       cy.get('#email').invoke('val', '')
//     }

//     if (password !== '') {
//       cy.get('#password').clear().type(password)
//     } else {
//       cy.get('#password').invoke('val', '')
//     }

//     if (password !== '') {
//       cy.get('#confirmPassword').clear().type(password)
//     } else {
//       cy.get('#confirmPassword').invoke('val', '')
//     }

//     cy.get('.CheckboxNext__CustomInput').click()
//     cy.get('.MyAccount-Buttons > .ButtonNext').click()
//   },
// )

// Cypress.Commands.add('smsConfirm', (code) => {
//   cy.get('#digit-0').type(code[0])
//   cy.get('#digit-1').type(code[1])
//   cy.get('#digit-2').type(code[2])
//   cy.get('#digit-3').type(code[3])
//   cy.get('.SmsVerificationForm-Submit').click()
// })

// Cypress.Commands.add('requestLogin', (existedUser) => {
//   cy.request(
//     'POST',
//     `${Cypress.env('ssoUrl')}/auth/signin/phone`,
//     existedUser,
//   ).then((response) => {
//     const { access_token, status } = response.body
//     // console.log(access_token)
//     expect(status).to.equal('SignIn success')
//     cy.request('POST', Cypress.env('graphqlUrl'), {
//       query:
//         'mutation ($_ssoAccessToken_0:String!) {generateCustomerTokenBySso(ssoAccessToken:$_ssoAccessToken_0){ customerToken, error }}',
//       variables: {
//         _ssoAccessToken_0: access_token,
//       },
//     }).then((response) => {
//       const {
//         customerToken,
//         error,
//       } = response.body.data.generateCustomerTokenBySso
//       expect(error).to.equal(null)
//       localStorage.setItem(
//         'auth_token',
//         JSON.stringify({
//           data: customerToken,
//           expiration: 3600,
//           createdAt: Date.now(),
//         }),
//       )
//       localStorage.setItem(
//         'sso_auth_token',
//         JSON.stringify({
//           data: access_token,
//           expiration: 3600,
//           createdAt: Date.now(),
//         }),
//       )
//     })
//   })
// })

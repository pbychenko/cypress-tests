Cypress.Commands.add('changePassword', (oldPassword, newPassword) => {
  cy.get('div.account-form__password > button > p').scrollIntoView().click()
  cy.get('.change-password-form').should('be.visible')
  cy.get('input[ name= "change-password-form__current_password"]').clear().type(oldPassword)
  cy.get('input[ name= "change-password-form__password"]').clear().type(newPassword)
  cy.get('input[ name= "change-password-form__confirm-password"]').clear().type(newPassword)
  cy.get('.change-password-form__confirm-button').click()
})

Cypress.Commands.add('remindPassword', (phone, password, sms) => {
  cy.get('.--account').click()
  cy.get('.sign-in-form__forgot-pass').click()
  cy.get('#forgot-password-form__phone').click().clear().type(phone)
  cy.get('.forgot-password-form__confirm-button').click()
  cy.get('#forgot-password-form__sms-code').should('be.visible')
  cy.get('#forgot-password-form__sms-code').type(sms)
  cy.get('#forgot-password-form__password').type(password)
  cy.get('#forgot-password-form__confirm-password').type(password)
  cy.get('.forgot-password-form__confirm-button').click()
})


import cartElements from "../pageElements/cartElements"
import catalogPageElements from '../pageElements/catalogPageElements'
import productPageElements from '../pageElements/productPageElements'

Cypress.Commands.add('addToCartFromPLP', (productNumber) => {
    cy.get(`${catalogPageElements.categoryItemCart}:nth-child(${productNumber}) ${catalogPageElements.addToCartButton}`).click({ force: true })
    cy.get(cartElements.addToCartModal).should('be.visible').contains('Вы добавили 1 товар в корзину')    
    cy.get(cartElements.goToCartFromModalButton).contains('Перейти в корзину').click()
})

Cypress.Commands.add('addToCartPdp', (sku) => {
    const productPageUrl = `/p/${sku}`
    cy.visit(productPageUrl)

    cy.get(productPageElements.addToCartButton).click()
    cy.get(cartElements.addToCartModal).should('be.visible').contains('Вы добавили 1 товар в корзину')    
    cy.get(cartElements.goToCartFromModalButton).contains('Перейти в корзину').click()
})

// Cypress.Commands.add('selectKovshGiftByNumber', (number) => {
//     cy.get(`.GiftBlock-Item:nth-child(${number}) .ButtonNext`).click()
// })

// export const parsePriceFromHtml = (price) => parseInt(price.replace(/(?!\w|\s)./g, '')
//   .replace(/\s+/g, ' ')
//   .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2').split(' ').slice(0, 2).join(''), 10);
export const parsePriceFromHtml = (price) => +price.split('\u00A0').join('').slice(0, -3);

export const isSortedList = (list, direction = 'asc') => {
  const result = direction === 'asc' ? (!!list.reduce((n, item) => n !== false && item >= n && item)) :
    (!!list.reduce((n, item) => n !== false && item <= n && item))
  return result;
};
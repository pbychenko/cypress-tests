import checkoutPageElements from '../pageElements/checkoutPageElements'

Cypress.Commands.add("selectCity", (city) => {
    cy.get('#ShippingStep__CityInput').clear().type(city)
    cy.get('#react-autowhatever-1--item-0').click()
})

Cypress.Commands.add("addNewAddress", (newAddress) => {
    cy.get(checkoutPageElements.otherDeliveryAddressButton).click({force:true})
    cy.get(checkoutPageElements.newAddressOrReceiverModal).should('be.visible').contains('Добавить адрес')
    cy.get(checkoutPageElements.newAddressStreetInput).should('be.enabled').clear().type(newAddress.street, {force: true}).should('have.value', newAddress.street)
    cy.get(checkoutPageElements.newAddressFloorInput).should('be.enabled').type(newAddress.floor, {force: true}).should('have.value', newAddress.floor)
    cy.get(checkoutPageElements.newAddressApartment).should('be.enabled').type(newAddress.apartment, {force: true}).should('have.value', newAddress.apartment)
    cy.get(checkoutPageElements.newAddressOrReceiverAddButton).should('be.visible').contains('Добавить').click()
})

Cypress.Commands.add("addNewReceiver", (newReceiver) => {    
    cy.get(checkoutPageElements.otherReceiverButton).scrollIntoView().click({force:true})    
    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.disabled')
    cy.get(checkoutPageElements.newReceiverFirstNameInput).should('be.enabled').type(newReceiver.name, {force: true}).should('have.value', newReceiver.name)
    // cy.get(checkoutPageElements.newReceiverLastNameInput).should('be.enabled').type(newReceiver.lastName, {force: true}).should('have.value', newReceiver.lastName)
    cy.get(checkoutPageElements.newReceiverPhoneInput).should('be.enabled').type(newReceiver.phone, {force: true})
    // cy.get(checkoutPageElements.newReceiverEmailInput).should('be.enabled').type(newReceiver.email, {force: true}).should('have.value', newReceiver.email)
    if (newReceiver.type === 'jur') {
      cy.get(checkoutPageElements.newJurReceiverOrgNameInput).should('be.enabled').type(newReceiver.organization.name, {force: true}).should('have.value', newReceiver.organization.name)
      cy.get(checkoutPageElements.newJurReceiverOrgAddressInput).should('be.enabled').type(newReceiver.organization.address, {force: true}).should('have.value', newReceiver.organization.address)
      cy.get(checkoutPageElements.newJurReceiverOrgBinInput).should('be.enabled').type(newReceiver.organization.bik, {force: true}).should('have.value', newReceiver.organization.bik)
      cy.get(checkoutPageElements.newJurReceiverOrgIicInput).should('be.enabled').type(newReceiver.organization.iic, {force: true}).should('have.value', newReceiver.organization.iic)
      cy.get(checkoutPageElements.newJurReceiverOrgBikInput).should('be.enabled').type(newReceiver.organization.bik, {force: true}).should('have.value', newReceiver.organization.bik)
      cy.get(checkoutPageElements.newReceiverLastNameInput).should('be.enabled').type(newReceiver.lastName, {force: true}).should('have.value', newReceiver.lastName)
      cy.get(checkoutPageElements.newReceiverEmailInput).should('be.enabled').type(newReceiver.email, {force: true}).should('have.value', newReceiver.email)
    }
    cy.get(checkoutPageElements.newAddressOrReceiverAddButton).should('be.visible').contains('Добавить').click()    
})

Cypress.Commands.add("enterBonusSms", (sms) => {    
    cy.get('.Verification .Verification__InputWrapper:nth-child(1) .Verification__Input').type(sms[0])
    cy.get('.Verification .Verification__InputWrapper:nth-child(2) .Verification__Input').type(sms[1])
    cy.get('.Verification .Verification__InputWrapper:nth-child(3) .Verification__Input').type(sms[2])
    cy.get('.Verification .Verification__InputWrapper:nth-child(4) .Verification__Input').type(sms[3])
})

Cypress.Commands.add("showInternalPromo", () => {
  for (let i = 0; i < 4; i += 1) {
    cy.get(checkoutPageElements.checkoutSummaryTitle).click({force: true})
  }

  cy.get(checkoutPageElements.internalPromoModal).should('be.visible').contains('Введите код сотрудника')    
})

Cypress.Commands.add("enterInternalCode", (code) => {
  cy.get(checkoutPageElements.internalPromoModalInput).type(code)
  cy.get(checkoutPageElements.internalPromoModalConfitm).click()
})
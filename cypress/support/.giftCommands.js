import cartElements from "../pageElements/cartElements"
import giftElements from "../pageElements/giftElements"


Cypress.Commands.add('orderGiftSet', (giftSet) => {
    const { link, main, gift, price, multiplicity } = giftSet
    cy.visit(link)
    cy.get(giftElements.addGiftSetToCart, {timeout:5000}).scrollIntoView().click()
    cy.get(giftElements.goToCartFromPopup, {timeout:5000}).click()
    cy.get(giftElements.cartItemMain).contains(main)
    cy.get(giftElements.cartItemGift).contains(gift)
    cy.get(giftElements.increase).click()
    cy.get(giftElements.orderButton).click()
    cy.get(giftElements.checkoutItemMain).contains(main)
    cy.get(giftElements.checkoutItemGift).contains(gift)

    if (multiplicity) 
        cy.get(giftElements.checkoutItems).should('have.length',1)
    else 
        cy.get(giftElements.checkoutItems).should('have.length',2)
        cy.get(giftElements.pickup).click()
        cy.get(giftElements.nextStep).click()
        cy.get(giftElements.price).contains(price)

        cy.get(giftElements.cashPayment).click()
        cy.get(giftElements.paymentButton).click()
        cy.contains('Ваш заказ оформлен, принимаемся за сборку заказа',{timeout:8000})
})
// import { cypressBrowserPermissionsPlugin } from 'cypress-browser-permissions'

// module.exports = (on, config) => {
//   // The plugin may modify the Cypress config, so be sure
//   // to return it
//   config = cypressBrowserPermissionsPlugin(on, config)

//   //
//   // Any existing plugins you are using
//   //

//   return config
// }
const { cypressBrowserPermissionsPlugin } = require('cypress-browser-permissions')

module.exports = (on, config) => {
  // The plugin may modify the Cypress config, so be sure
  // to return it
  config = cypressBrowserPermissionsPlugin(on, config)

  //
  // Any existing plugins you are using
  //

  return config
}

//ref: https://docs.cypress.io/api/plugins/browser-launch-api.html#Usage
module.exports = (on, config) => {
  on('before:browser:launch', (browser = {}, launchOptions) => {
    if (browser.name === 'chrome') {
      launchOptions.args.push('--disable-dev-shm-usage')
    }
  
    return launchOptions
  })
}
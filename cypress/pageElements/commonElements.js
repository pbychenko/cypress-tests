const commonElements = {
  notification: '.Message',
  cartIcon:'.--cart',
  cartIconCounter:'.--cart data',
  bonusSection: '.bonuses__amount',
  accountName: '.account__firstname',
};

export default commonElements;
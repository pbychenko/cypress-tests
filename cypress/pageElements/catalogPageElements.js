const catalogPageElements = {
    categoryItemCart: '.category-page-list__item',
    productPrice: '.ProductCardV__Price',
    productTitle: '.ProductCardV__Title',
    addToCartButton: '.--addToCart',
    categorySortSelector: '.category-page-list__sort-select',
    sortNameSelector: '.category-page-list__sort-value-label',
    categorySortByAscSelector: '#react-select-2-option-2',
    categorySortByDescSelector: '#react-select-2-option-3',
    paginationLink: '.Paginator__List > div',
    minPriceField: '#priceFilterFrom',
    maxPriceField: '#priceFilterTo',
    filtersPanelElement: '.category-page-list__active-filters div',
    breadcrumbs: '.BreadcrumbsNext__Crumb',
    filtersSection: '.category-filters__list',
    categoryFilterLink: '.category-filters__tag-link'

    // // priceRangeButton: '.CategoryShoppingOptions-PriceRangeButton', - такой кнопки пока нет в новом PLP
    // selectedFiltersItem: '.CategoryFilters .TagNext_Active p',
    // selectedPageListItem: '.CategoryPageList .TagNext_Active p',
    

};

export default catalogPageElements;
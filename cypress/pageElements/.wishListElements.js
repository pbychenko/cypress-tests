const wishListElements = {
    productCard: '.MyAccountPage-Content .ProductCard',
    deleteProduct: '.MyAccountPage-Content .ProductCard .ProductWishlistButton-Button_isShownInWishlist',
    deleteAll: '.MyAccountPage-Content > .MyAccountWishlist .MyAccountWishlist-Link',
};

export default wishListElements;
const myAccountAddressPageElements = {
    addressReceiverBlock: '.receivers-and-addresses__receiver',
    lastAddressReceiverTitle:
      '.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__name',
    lastAddressSubTitle:
      '.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__address',
    lastReceiverEmailSubTitle:
      '.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__email',
    lastAddressReceiverDeleteButton:
      '.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__delete-button',
    lastAddressReceiverBlockEditButton:
      '.receivers-and-addresses__list > .receivers-and-addresses__receiver:last-child .receivers-and-addresses__change-button',
}

export default myAccountAddressPageElements

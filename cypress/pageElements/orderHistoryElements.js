const orderHistoryElements = {
  orderStatus:
    'a .orders-table__cell .StickerNext  p',
  statusFilter: '#orderStatuses',
  calendar: '.CalendarNext',
  dateInput: '#dateFromDateTo',
  dateMonth: '.CalendarNext__HeaderDate',
  prevMonth: '.CalendarNext__HeaderButton_Prev',
  orderRow: '.orders-table .orders-table__row.--body',
  selectedStatus: '.SelectNext__single-value'
}

export default orderHistoryElements

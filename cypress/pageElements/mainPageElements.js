const mainPageElements = {
   accountIcon: '.--account',
   modalCityButton:
      '.verify-city-modal__actions > button.ButtonNext_Theme-Alpha',
   toastMessage: '.Typography.Message__Content.Typography__Body'
}

export default mainPageElements

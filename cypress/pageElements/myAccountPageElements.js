const myAccountPageElements = {
  overviewUrl: '/my-account/overview',
  logout: '.--exit',
  forgotPass: '.sign-in-form__forgot-pass',
  phoneField: '#forgot-password-form__phone',
  confirmPhoneField: '.forgot-password-form__confirm-button',
  accountData: {
    editPassword: '.account-form__password > button',
    passwordForm: 'form.forgot-password-form',
    editPasswordField: '#forgot-password-form__password',
    confirmPasswordField: '#forgot-password-form__confirm-password',
    savePassword: '.forgot-password-form__confirm-button',
    firstname: '#userName',
    lastname: '#userLastname',
    email: '#userEmail',
    language: '.--lang',
    gender: '.--sex',
    dateOfBirth: '#userBirthDate',
    save: '.account-form__submit'
  },
  bonusCard: {
    bonusStatus: '.StickerNext',
    progress: '.bonus__progress',
    salesAmount: '.bonus__sum-item-accumulated',
    bonusInfoBlock: '.bonus__info',
    bonusInfoRow: '.bonus__label'
  },
}

export default myAccountPageElements

const compareElements = {
    compareIcon:'.--compare',
    compareIconCounter:'.--compare data',
    productCard: '.compare__product-item',
    deleteProduct: '.--addToCart',
    addToCard: '.--goToCheckout',
    characteristics: '.characteristic-item',    
};

export default compareElements;
import catalogPageElements from '../pageElements/catalogPageElements'
import '../support/catalogPageCommands';
import { parsePriceFromHtml, isSortedList } from '../support/utils'

const categoryUrl = '/catalog/bytovaja-tehnika/hranenie-produktov-i-napitkov/holodil-niki'
const holodilnikiRelatePath = 'bytovaja-tehnika/hranenie-produktov-i-napitkov/holodil-niki'
const categoryName = 'holodil-niki'      

describe('catalog tests', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true') 
    cy.visit(categoryUrl)
  })

  it('check sorting by asc', () => {
    cy.get(catalogPageElements.sortNameSelector).contains('популярности')

    cy.selectSorting('asc', categoryName)
    cy.get(catalogPageElements.sortNameSelector).contains('возрастанию цены')

    const prices = []
    cy.get(`${catalogPageElements.categoryItemCart} ${catalogPageElements.productPrice}`).each((el) => {
      cy.wrap(el).invoke('text').then((value) => {
        prices.push(parsePriceFromHtml(value))
      })
    }).then(() => {
      expect(isSortedList(prices, 'asc')).to.equal(true)
    })   
  })

  it('check sorting by desc', () => {
    cy.selectSorting('desc', categoryName)
    cy.get(catalogPageElements.sortNameSelector).contains('убыванию цены')

    const prices = []
    cy.get(`${catalogPageElements.categoryItemCart} ${catalogPageElements.productPrice}`).each((el) => {
      cy.wrap(el).invoke('text').then((value) => {
        prices.push(parsePriceFromHtml(value))
      })
    }).then(() => {
      expect(isSortedList(prices, 'desc')).to.equal(true)
    })   
  })

  it('check sorting after pagination', () => {
    cy.get(`${catalogPageElements.paginationLink}:nth-of-type(1) .Paginator__List-Item`).should('have.class', '--active').contains('1')
    
    cy.selectSorting('asc', categoryName)
    cy.get(`${catalogPageElements.paginationLink}:nth-of-type(2)`).click()
    
    cy.get(`${catalogPageElements.paginationLink}:nth-of-type(2) .Paginator__List-Item`).should('have.class', '--active').contains('2')
    cy.get(catalogPageElements.sortNameSelector).contains('возрастанию цены')

    const prices = []
    cy.get(`${catalogPageElements.categoryItemCart} ${catalogPageElements.productPrice}`).each((el) => {
      cy.wrap(el).invoke('text').then((value) => {
        prices.push(parsePriceFromHtml(value))
      })
    }).then(() => {
      expect(isSortedList(prices, 'asc')).to.equal(true)
    })
  })

  it('check filter by pricing', () => {
    const minPrice = 70000
    const maxPrice = 170000

    cy.fillPriceFilters(minPrice, maxPrice, categoryName)   

    cy.get(catalogPageElements.filtersPanelElement).should('have.length', 1).contains('Цена')
    cy.get(`${catalogPageElements.categoryItemCart} ${catalogPageElements.productPrice}`).each((el) => {
      cy.wrap(el).invoke('text').then((value) => {
        const priceTodigit = parsePriceFromHtml(value)
        expect(priceTodigit >= minPrice && priceTodigit <= maxPrice).to.equal(true)
      })
    })
  })

  it('check filter by brand', () => {
    const brand = 'Beko'
    cy.selectBrandInFilters(brand, categoryUrl)

    cy.get(catalogPageElements.filtersPanelElement).contains(brand)
    cy.get(`${catalogPageElements.categoryItemCart} ${catalogPageElements.productTitle}`).each((el) => {
      cy.wrap(el).contains(brand)      
    })
  })

  it('check breadcrumbs', () => {
    cy.get(catalogPageElements.breadcrumbs).should('have.length', 4)
    cy.get(`${catalogPageElements.breadcrumbs}:last-child`).contains('Холодильники')
    cy.get('h1').contains('Холодильники в Алматы')

    cy.get(`${catalogPageElements.breadcrumbs}:nth-child(3)`).contains('Хранение продуктов и напитков').click()
    cy.get('h1').contains('Хранение продуктов и напитков в Алматы')
    cy.get(catalogPageElements.breadcrumbs).should('have.length', 3)
    cy.get(catalogPageElements.filtersSection).contains('Подкатегории')

    const holodilnikiPartUrl = categoryUrl.split('/')[-1]
    cy.get(`[href*="${holodilnikiRelatePath}"]`).contains('Холодильники').click()
    cy.get(catalogPageElements.breadcrumbs).should('have.length', 4)
    cy.get('h1').contains('Холодильники в Алматы')

    cy.get(`${catalogPageElements.breadcrumbs}:first-child`).contains('Главная').click()
    cy.url().should('eq', Cypress.config().baseUrl)
  })
})

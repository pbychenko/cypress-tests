import myAccountAddressPageElements from '../pageElements/myAccountAddressPageElements'
import '../support/addressReceiverPageCommands'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}

const physReceiver = {
  firstName: 'PhysName',
  lastName: 'PhysLastName',
  phone: '7015054921',
  email: 'phys@test.org',
}

const jurData = {
    orgName: 'Technodom',
    orgBIN: 'БИН',
    orgBIK: 'БИК',
    orgAddress: 'юр адрес',
    orgIIC: 'ИИК',
}

const deleteReceiverById = (id, authorization) => {
  const deleteOptions = {
    method: 'Delete',
    url: `${Cypress.env('ssoUrlV2')}/profile/receivers/${id}`,
    headers: {
      authorization,
    }};
  cy.request(deleteOptions).its('status').should('eq', 204)
}

const getIntercept = () => {
  const api = `${Cypress.env('ssoUrlV2')}/profile/receivers`
  cy.intercept(api).as('getReceivers')
}

const clearReceivers = (authorization) => {
  const options = {
    method: 'GET',
    url: `${Cypress.env('ssoUrlV2')}/profile/receivers`,
    headers: {
      authorization,
    }
  };

  cy.request(options).then(({ body }) => {
    expect(body).to.have.property('receivers')
    const ids = body.receivers.map(el => el.id)
    ids.forEach((id) => {
      deleteReceiverById(id, authorization)
    })
  })
}

describe('actions with receivers', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')

    cy.requestLogin(existedUser).then(() => {
      const token = Cypress.env('token')
      const authorization = `Bearer ${ token }`
      clearReceivers(authorization)
    })
          
    cy.visit('/my-account/address-book')
    cy.contains('Адресная книга') 
  })

  it('add and delete new phys receiver', () => {
    const {
      addressReceiverBlock,
      lastAddressReceiverTitle,
      lastReceiverEmailSubTitle,
      lastAddressReceiverDeleteButton
    } = myAccountAddressPageElements

    cy.addReceiver(physReceiver, true)

    cy.get(lastAddressReceiverTitle).scrollIntoView()
      .should('be.visible')
      .contains(`${physReceiver.firstName} ${physReceiver.lastName}`)
    cy.get(lastReceiverEmailSubTitle)
      .should('be.visible')
      .contains(`${physReceiver.email}`)

    cy.get(addressReceiverBlock).then((elements) => {
      const addressesCountBeforeDeletion = elements.length
      cy.get(lastAddressReceiverDeleteButton).click()
      getIntercept()
      cy.wait('@getReceivers').its('response.statusCode').should('eq', 200)

      cy.get('body').then(($body) => {
        const innerElements = $body.find(addressReceiverBlock)
        const addressesCountAfterDeletion = innerElements.length
        
        expect(true).to.equal(
          addressesCountBeforeDeletion - 1 === addressesCountAfterDeletion,
        )
      })
    })
  })

  // it('delete phys receiver', () => {
  //   const {
  //     addressReceiverBlock,
  //     lastAddressReceiverDeleteButton,
  //   } = myAccountAddressPageElements

  //   cy.addReceiver(physReceiver, true)

  //   cy.get(addressReceiverBlock).then((elements) => {
  //     const addressesCountBeforeDeletion = elements.length
  //     cy.get(lastAddressReceiverDeleteButton).click()
  //     cy.wait(1000)
  //     cy.get('body').then(($body) => {
  //       const innerElements = $body.find(addressReceiverBlock)
  //       const addressesCountAfterDeletion = innerElements.length
        
  //       expect(true).to.equal(
  //         addressesCountBeforeDeletion - 1 === addressesCountAfterDeletion,
  //       )
  //     })
  //   })
  // })

  it('edit phys receiver', () => {
    const {
      lastAddressReceiverTitle,
      lastReceiverEmailSubTitle,
    } = myAccountAddressPageElements
    const newReceiver = {
      firstName: 'PhysName1',
      lastName: 'PhysLastName1',
      phone: '7015054922',
      email: 'phys1@test.org',
      jurData: {},
    }

    cy.addReceiver(physReceiver, true)
    cy.contains('Адресная книга')
    
    cy.editReceiver(newReceiver)

    cy.get(lastAddressReceiverTitle).scrollIntoView()
      .should('be.visible')
      .contains(`${newReceiver.firstName} ${newReceiver.lastName}`)
    cy.get(lastReceiverEmailSubTitle)
      .should('be.visible')
      .contains(`${newReceiver.email}`)
  })

  it('change type of receiver', () => {
    const {
      lastAddressReceiverTitle,
      lastReceiverEmailSubTitle,
    } = myAccountAddressPageElements

    cy.addReceiver(physReceiver, true)
    cy.contains('Адресная книга')

    cy.moveReceiverToJurType(jurData)

    cy.get(lastAddressReceiverTitle).scrollIntoView()
      .should('be.visible')
      .contains(`${jurData.orgName}`)
    cy.get(lastReceiverEmailSubTitle)
        .should('be.visible')
        .contains(`${physReceiver.email}`)
  })  
})

import '../support/cartCommands'
import '../support/productActionsCommands'
import cartElements from "../pageElements/cartElements"
import commonElements from '../pageElements/commonElements'

const categoryUrl = '/catalog/bytovaja-tehnika/hranenie-produktov-i-napitkov/holodil-niki'
const categorySeoTitle = 'Холодильники в Алматы'
const skus = ['216442', '235694', '215507']

describe('cart tests', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
  })

  it('add to cart from PLP', () => {
    cy.visit(categoryUrl)
    cy.contains(categorySeoTitle)

    cy.addToCartFromPLP(4)

    cy.contains('Корзина')
    cy.get(cartElements.cartItem).should('be.visible')
    cy.get(commonElements.cartIconCounter).contains(1)
  })

  it('add to cart from PDP', () => {
    cy.addToCartPdp(skus[0])

    cy.contains('Корзина')
    cy.get(cartElements.cartItem).should('be.visible')
    cy.get(commonElements.cartIconCounter).contains(1)
  })
  
  it('increase/decrease quantity', () => {
    cy.visit('/')
    cy.addToGuestCartViaApi(skus[0])
    cy.visit('/cart')
    cy.get(cartElements.cartItem).should('be.visible')

    cy.get(cartElements.increaseButton).click()
    cy.get(cartElements.qtyInputField).should('have.value', 2)
    cy.get(commonElements.cartIconCounter).contains(2)

    cy.get(cartElements.decreaseButton).click()
    cy.get(cartElements.qtyInputField).should('have.value', 1)
    cy.get(commonElements.cartIconCounter).contains(1)
  })

  it('change quantity via input', () => {
    cy.visit('/')
    cy.addToGuestCartViaApi(skus[0])
    cy.visit('/cart')
    cy.get(cartElements.cartItem).should('be.visible')

    cy.get(cartElements.qtyInputField).clear().type('5').should('have.value', '5')
    cy.get(cartElements.cartItem).click()
    cy.get(commonElements.cartIconCounter).contains(5)
  })

  it('delete product from cart and clear cart', () => {
    cy.visit('/')

    skus.forEach(sku => {
      cy.addToGuestCartViaApi(sku)
    })

    cy.visit('/cart')
    cy.get(cartElements.cartItem).should('have.length', skus.length)
    cy.get(commonElements.cartIconCounter).contains(skus.length)

    cy.get(`${cartElements.cartItem}:nth-of-type(2) ${cartElements.deleteFromCartButton}`).click()
    cy.get(commonElements.cartIconCounter).contains(skus.length - 1)

    cy.get(cartElements.clearCartButton).click()
    cy.get(cartElements.cartItem).should('not.exist')
    cy.get(commonElements.cartIconCounter).should('not.exist')
    cy.contains('У вас нет товаров в корзине')
  })
})



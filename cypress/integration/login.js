import mainPageElements from '../pageElements/mainPageElements'
import myAccountPageElements from '../pageElements/myAccountPageElements'
import commonElements from '../pageElements/commonElements'
import '../support/passwordCommands'
const unregisteredPhone = '7774521628'
const registeredPhone =  '7773411235'
const correctPassword = 'qweqweqwe'
const incorrectPassword = 'qweqweqwe1'

const existedUser = {
  phone: '7773411235',
  password: 'qweqweqwe',
}

describe('login via ui', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.visit('/')
    // if(cy.get(mainPageElements.modalCityButton)){
    //   cy.get(mainPageElements.modalCityButton).click()
    // }

  })

  it('login with correct data', () => {
    cy.uiLogin(registeredPhone, correctPassword)
    cy.contains('Вы успешно вошли в систему!')
    cy.get(commonElements.notification).contains('Вы успешно вошли в систему')
    // cy.get(myAccountPageElements.accountName).should('be.visible')
  })

  it('login with incorrect data', () => {
    cy.uiLogin(registeredPhone, incorrectPassword)
    cy.get(commonElements.accountName).should('not.exist')
    cy.get(commonElements.notification).contains('Неверный номер телефона или пароль')
  })

  it('logout', () => {
    cy.uiLogin(registeredPhone, correctPassword)
    cy.contains('Вы успешно вошли в систему!')
    cy.get(commonElements.accountName).should('exist')
    cy.visit(myAccountPageElements.overviewUrl)
    cy.contains('Мой профиль')
    cy.get(myAccountPageElements.logout).click()
    cy.get(commonElements.accountName).should('not.exist')
  })

  //хз как проверять чтобы точно не было телефона в новой базе

  // it('login with unregistered phone', () => {
  //   cy.uiLogin(unregisteredPhone, correctPassword)
  //   cy.get(commonElements.notification).contains('Неверный номер телефона или пароль')
  // })

  // it('remind password with correct sms', () => {
  //   cy.remindPassword(registeredPhone, correctPassword, '1111')
  //   cy.get(commonElements.notification).contains('Пароль успешно восстановлен')
  // })

  // it('remind password unregistered user', () => {
  //   cy.get(mainPageElements.accountIcon).click()
  //   cy.get(myAccountPageElements.forgotPass).click()
  //   cy.get(myAccountPageElements.phoneField).clear().type(unregisteredPhone)
  //   cy.get(myAccountPageElements.confirmPhoneField).click()
  //   cy.get(commonElements.notification).contains('Пользователь с таким номером не зарегистрирован')
  // })
})

import productPageElements from '../pageElements/productPageElements'
import commonElements from '../pageElements/commonElements'
import '../support/pdpCommands'

const sku = '216442'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}

const getIntercept = () => {
  const api = `https://stapi.technodom.kz/feedback/api/v1/reviews/product/*`
  cy.intercept(api).as('getReviews')
}

describe('pdp elements', function() {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.visit(`/p/${sku}`)
  })

  it('check availability of main pdp elements', () => {
    cy.get(productPageElements.addToCartButton).should('be.visible')
    cy.get(productPageElements.shareLinkButton).should('be.visible')
    cy.get(productPageElements.deliveryInformationBlock).should('be.visible')
    cy.get(productPageElements.pickupInformationBlock).should('be.visible')
    cy.get(productPageElements.shortProductInfoBlock).should('be.visible')
    cy.get(productPageElements.reesBlock).should('have.length.at.least', 3)

    cy.get(productPageElements.productInfoButton).should('be.visible').click()
    cy.get(productPageElements.productInfoTab).should('be.visible').should('have.class', '--active')

    cy.get(productPageElements.stocksTab).click()
    cy.get(productPageElements.stocksBlock).should('be.visible')
  })

  it('check share link functionality', () => {
    cy.get(productPageElements.shareLinkButton).should('be.visible').contains('Поделиться').click()

    cy.get(productPageElements.productShareBlock).should('be.visible').contains('Вконтакте')
    cy.get(productPageElements.productShareCopyButton).should('be.visible').contains('Скопировать ссылку').click()

    cy.get(commonElements.notification).should('be.visible').contains('Ссылка скопирована')
  })

  it('check review functionality', () => { 
    cy.get(productPageElements.productInfoButton).should('be.visible').click()
    cy.get(productPageElements.reviewsTab).should('be.visible').click()
    cy.get(productPageElements.addReviewButton).should('be.visible').click()
    cy.get(commonElements.notification).should('be.visible').contains('Пожалуйста, авторизуйтесь')

    cy.requestLogin(existedUser)
    cy.visit(`/p/${sku}`)

    cy.get(productPageElements.reviewsTab).scrollIntoView().click()
    cy.get(productPageElements.addReviewButton).should('be.visible').click()
    
    cy.get(productPageElements.productReviewBlock).should('be.visible')
    cy.get(productPageElements.submitReviewButton).click()

    cy.get(productPageElements.errorInputMessageField).should('be.visible').contains('Это необходимое поле!')
    cy.get(productPageElements.errorRatingMessageField).should('be.visible').contains('Это необходимое поле!')

    cy.get(productPageElements.reviewInputField).clear().type('test')
    cy.get(productPageElements.submitReviewButton).click()
    cy.get(productPageElements.errorInputMessageField).should('be.visible').contains('Минимальное количество символов 5')

    //это можно будет расскомменить после того как сделаются апи методы для удаления отзывов
    // cy.get(productPageElements.reviewInputField).clear().type('Это тестовый отзыв')
    // cy.get(`${productPageElements.ratingField} > span:nth-of-type(2)`).click()
    // cy.get(productPageElements.submitReviewButton).click()

    // cy.get(commonElements.notification).should('be.visible').contains('Отзыв был отправлен на модерацию')
  })
})
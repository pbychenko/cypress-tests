import orderHistoryElements from '../pageElements/orderHistoryElements'
import commonElements from '../pageElements/commonElements'
import myAccountPageElements from '../pageElements/myAccountPageElements'
describe('orders history', () => {
  const existedUser = {
    phone: '7773411235',
    password: 'qweqweqwe',
  }

  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.requestLogin(existedUser)    
    cy.visit(myAccountPageElements.overviewUrl)
  })

  it('order by status filter', () => {    
    cy.get(commonElements.accountName).should('be.visible')
    cy.visit('/my-account/orders-list')
    cy.get(orderHistoryElements.orderRow).should('be.visible')
    //тест выбирает Завершен, а проверяет подтвержден. Нужно переписать тест
    
    // cy.get(orderHistoryElements.statusFilter).type('{downarrow}{downarrow}{downarrow}{enter}', { force: true })
    // cy.wait(2000)
    // cy.get(orderHistoryElements.orderRow).each(
    //   ($order, index, $list) => {
    //     cy.wrap($order)
    //       .find(orderHistoryElements.orderStatus)
    //       .should('contain.text', 'Подтвержден')
    //   },
    // )
  })

})

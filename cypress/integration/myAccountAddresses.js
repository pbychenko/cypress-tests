import myAccountAddressPageElements from '../pageElements/myAccountAddressPageElements'
import '../support/addressReceiverPageCommands'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}
const address = {
  city: 'Шымкент',
  shortAddress: 'Манаса,59',
  floorNumber: 1,
  flatNumber: 2
}

const deleteAddressById = (id, authorization) => {
  const deleteOptions = {
    method: 'Delete',
    url: `${Cypress.env('ssoUrlV2')}/profile/addresses/${id}`,
    headers: {
      authorization,
    }};
  cy.request(deleteOptions).its('status').should('eq', 204)
}

const getIntercept = () => {
  const api = `${Cypress.env('ssoUrlV2')}/profile/addresses`
  cy.intercept(api).as('getAddresses')
}

const clearAddresses = (authorization) => {
  const options = {
    method: 'GET',
    url: `${Cypress.env('ssoUrlV2')}/profile/addresses`,
    headers: {
      authorization,
    }
  };

  cy.request(options).then(({ body }) => {
    expect(body).to.have.property('addresses')
    const ids = body.addresses.map(el => el.id)
    if(ids.length !== 0) {
      ids.forEach((id) => {
        deleteAddressById(id, authorization)
      })
    }    
  })
}

describe('actions with addresses', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')

    cy.requestLogin(existedUser).then(() => {
      const token = Cypress.env('token')
      const authorization = `Bearer ${ token }`
      clearAddresses(authorization)
    })
          
    cy.visit('/my-account/address-book')
    cy.contains('Адресная книга') 
  })

  it('add and delete new address', () => {
    const {
      lastAddressReceiverTitle,
      lastAddressSubTitle,
      addressReceiverBlock,
      lastAddressReceiverDeleteButton
    } = myAccountAddressPageElements
  
    cy.addAddress(address)

    cy.get(lastAddressReceiverTitle).scrollIntoView()
      .should('be.visible')
      .contains(`${address.shortAddress}`)
    cy.get(lastAddressSubTitle)
      .should('be.visible')
      .contains(`кв. ${address.flatNumber}, ${address.floorNumber} этаж, ${address.city}`)

    cy.get(addressReceiverBlock).then((elements) => {
      const addressesCountBeforeDeletion = elements.length
      cy.get(lastAddressReceiverDeleteButton).click()
      getIntercept()
      cy.wait('@getAddresses').its('response.statusCode').should('eq', 200)    
      
      cy.get('body').then(($body) => {
        const innerElements = $body.find(addressReceiverBlock)
        const addressesCountAfterDeletion = innerElements.length
        
        expect(true).to.equal(
          addressesCountBeforeDeletion - 1 === addressesCountAfterDeletion,
        )
      })
    })
  })

  // it('delete address', () => {
  //   const {
  //     addressReceiverBlock,
  //     lastAddressReceiverDeleteButton,
  //   } = myAccountAddressPageElements
  
  //   cy.addAddress(address)
  //   cy.contains('Адресная книга')

  //   cy.get(addressReceiverBlock).then((elements) => {
  //     const addressesCountBeforeDeletion = elements.length
  //     cy.get(lastAddressReceiverDeleteButton).click()
  //     cy.wait(1000)
  //     cy.get('body').then(($body) => {
  //       const innerElements = $body.find(addressReceiverBlock)
  //       const addressesCountAfterDeletion = innerElements.length
        
  //       expect(true).to.equal(
  //         addressesCountBeforeDeletion - 1 === addressesCountAfterDeletion,
  //       )
  //     })
  //   })
  // })

  it('edit address', () => {
    const {
      lastAddressReceiverTitle,
      lastAddressSubTitle,
    } = myAccountAddressPageElements
    const newAddress = {
      city: 'Караганда',
      shortAddress: 'Манаса,58',
      floorNumber: 3,
      flatNumber: 4
    }

    cy.addAddress(address)
    cy.contains('Адресная книга')    
    
    cy.editAddress(newAddress)

    cy.get(lastAddressReceiverTitle).scrollIntoView()
      .should('be.visible')
      .contains(`${newAddress.shortAddress}`)
    cy.get(lastAddressSubTitle)
      .should('be.visible')
      .contains(`кв. ${newAddress.flatNumber}, ${newAddress.floorNumber} этаж, ${newAddress.city}`)
  }) 
})

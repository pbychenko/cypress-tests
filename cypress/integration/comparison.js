import compareElements from'../pageElements/compareElements';
import productPageElements from '../pageElements/productPageElements'
import cartElements from '../pageElements/cartElements'
import commonElements from '../pageElements/commonElements'
import '../support/pdpCommands'

const sku1 = '244587'
const sku2 = '248492'
const sku3 = '233016'


describe('Add to compare functionality', function() {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
  })

  it('add to compare from product page and delete it via comparison page', function() {
    cy.addProductToCompare(sku1)

    cy.get(productPageElements.addToCompareButton).click()
    cy.get(commonElements.notification).should('contain.text', 'Данный товар уже есть в списке сравнения')

    cy.get(compareElements.compareIconCounter).contains(1)

    cy.visit('/compare')
    cy.get(compareElements.addToCard).should('be.visible').contains('В корзину')
    cy.get(compareElements.characteristics).should('be.visible')
    cy.get(compareElements.productCard).should('have.length', 1)

    cy.get(compareElements.deleteProduct).contains('Убрать').click()
    cy.get(compareElements.productCard).should('have.length', 0)
    cy.contains('Нет продуктов для сравнения')     
  })

  it('try to add 2 products of different categories to compare', function() {
    cy.addProductToCompare(sku1)

    cy.visit(`/p/${sku2}`)
    cy.get(productPageElements.addToCompareButton).should('be.visible').click()
    cy.get(commonElements.notification).should('contain.text', 'Категория продукта не подходит для сравнения. Хотите очистить страницу "Сравнения товаров"?')
    cy.get(compareElements.compareIconCounter).contains(1)     
  })

  it('add 2 products of the same categories to compare', function() {
    cy.addProductToCompare(sku1)
    cy.addProductToCompare(sku3)

    cy.get(compareElements.compareIconCounter).contains(2)

    cy.visit('/compare')
    cy.get(compareElements.productCard).should('have.length', 2)
  })

  it('add to cart from compare', function() {
    cy.addProductToCompare(sku1)
    
    cy.visit('/compare')
    cy.get(compareElements.addToCard).click()
    cy.get(commonElements.notification).should('contain.text', 'Вы добавили 1 товар в корзину')

    cy.visit('/cart')
    cy.get(cartElements.cartItem).should('be.visible')
    cy.get(commonElements.cartIconCounter).contains(1)
  })
})
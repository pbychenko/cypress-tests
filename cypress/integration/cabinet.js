import myAccountPageElements from '../pageElements/myAccountPageElements'
import commonElements from '../pageElements/commonElements'
import '../support/passwordCommands'
import '../support/accountActionsCommands'

describe('cabinet tests', () => {
  const oldPassword = 'qweqweqwe'
  const newPassword = 'qweqweqwe1'


  const nameOld = 'ШАХНАЗА'
  const lastNameOld = 'ИМАНБЕРДИЕВА'
  const emailOld = 'imanberdiyeva@gmail.com'
  
  
  const emailNew = 'pupkin984797@mail.ru'
  const nameNew = 'Иван'
  const lastNameNew = 'Пупкин'

  const existedUser = {
    phone: '+77773411235',
    password: 'qweqweqwe',
  }

  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.requestLogin(existedUser)

    cy.visit(myAccountPageElements.overviewUrl)
    cy.get(commonElements.accountName).should('exist')
  })

  it('change password', () => {
    cy.changePassword(oldPassword, newPassword)
    cy.get(commonElements.notification).contains('Пароль успешно изменен')
    cy.changePasswordViaApi(newPassword, oldPassword)
  })

  it('check elements', () => {
    //временно задизаблил, у этого профиля не показывается карта (соотв задачи еще нет в master) и кажется локатор карты неверный
    // cy.get(myAccountPageElements.bonusCard.bonusStatus).should('be.visible')
    cy.get(myAccountPageElements.bonusCard.progress).should('be.visible')
    cy.get(myAccountPageElements.bonusCard.salesAmount).should('be.visible')
    cy.get(myAccountPageElements.bonusCard.bonusInfoBlock).should('be.visible')
    cy.get(myAccountPageElements.bonusCard.bonusInfoRow).should('have.length', 4)
    cy.get(myAccountPageElements.accountData.firstname).should('have.value', nameOld)
    cy.get(myAccountPageElements.accountData.lastname).should('have.value', lastNameOld)
    cy.get(myAccountPageElements.accountData.email).should('have.value', emailOld)
    cy.get(myAccountPageElements.accountData.language).scrollIntoView().should('be.visible')
    cy.get(myAccountPageElements.accountData.dateOfBirth).should('be.visible')
    cy.get(myAccountPageElements.accountData.gender).should('be.visible')
    cy.get(myAccountPageElements.accountData.save).should('be.visible')   
  })

  it('change personal data', () => {
    cy.get(commonElements.accountName).contains(nameOld)
    cy.get(myAccountPageElements.accountData.firstname).should('have.value', nameOld)
    cy.get(myAccountPageElements.accountData.firstname).clear().type(nameNew)
    cy.get(myAccountPageElements.accountData.lastname).clear().type(lastNameNew)    
    cy.get(myAccountPageElements.accountData.email).clear().type(emailNew)
    cy.get(myAccountPageElements.accountData.save).click()

    //Пока есть баг, связанный с изменения именно в этом аккаунте
    // cy.get(commonElements.accountName).contains(nameNew)
    cy.get(commonElements.notification).contains('Данные успешно сохранены')

    cy.get(myAccountPageElements.accountData.firstname).should('have.value', nameNew)
    cy.get(myAccountPageElements.accountData.lastname).should('have.value', lastNameNew)
    cy.get(myAccountPageElements.accountData.email).should('have.value', emailNew)

    cy.getProfileTdid()

    const userData = {
      firstname: nameOld,
      lastName: lastNameOld,
      email: emailOld,
      tdid: Cypress.env('tdid')
    }

    cy.updateProfile(userData)
  })
})

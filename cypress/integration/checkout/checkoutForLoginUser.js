import '../../support/productActionsCommands'
import '../../support/accountActionsCommands'
import '../../support/checkoutCommands'
import checkoutPageElements from '../../pageElements/checkoutPageElements'
import commonElements from '../../pageElements/commonElements'
import { parsePriceFromHtml } from '../../support/utils'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}

const sku = '235694'
const product2 = { sku:'216686', cost: 100 }

const receiver = {
  email:"test1@gmail.com",
  firstname: 'tf',
  id: '',
  isDefault: true,
  isOrganization: false,
  lastname: 'tl',
  organization: {
    address: '',
    bik: '',
    bin: '',
    iic: '',
    name: '',
  },
  phone: '+7 705 403 92 10',
}

const address = {
  apartment: '2',
  city: 'Алматы',
  corpus: '',
  floor: '1',
  geo: {
    lat: "43.203014",
    lng:"76.897872",
  },
  house: '100',
  id: '',
  isDefault: true,
  region: {
    code: '',
    id: 0,
    name: ''
  },
  street: 'проспект Гагарина',
  zipcode: '050000'
}

const newAddress = {
  street: 'улица Шевченко, 154',
  floor: 2,
  apartment: 3,
}

const newReceiver = {
  type: 'phys',
  name: 't2f',
  lastName: 't2l',
  phone: '7054039211',
  email: 'test2@gmail.com'
}

const shopAddress = 'Гагарина, 124'
const pvzAddress = 'ул. Курмангазы, 178А'

const paymentMethodsTitles = ['Картой онлайн', 'Оплата при получении', 'Рассрочка / кредит']

describe('checkout for login user', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.requestLogin(existedUser).then(() => {
      cy.clearReceivers()
      cy.addReceiverToAccountViaApi(receiver)

      cy.clearAddresses()
      cy.addAddresToAccountViaApi(address)

      cy.clearUserCartViaApi()
      cy.addToUserCartViaApi(sku)
    })  
  })

  it('creation new address and receiver via on checkout and change address and receiver', () => {
    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)
    cy.get(checkoutPageElements.deliveryAddressItemSection).contains(`${address.street}, ${address.house}`)
    cy.get(checkoutPageElements.deliveryAddressItemSection).contains(`Кв. ${address.apartment}, этаж ${address.floor}`)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).contains(`${receiver.firstname} ${receiver.lastname}`)
    cy.get(checkoutPageElements.receiverItemSection).contains(receiver.email)
    cy.get(checkoutPageElements.receiverItemSection).contains(receiver.phone)

    cy.get(checkoutPageElements.moveToPaymentsButton).click()
  
    cy.url().should('include', checkoutPageElements.checkoutPaymentUrl)
      // cy.contains('cпособ оплаты') - надо добавить, когда пофиксят баг с кнопкой вернуться
    cy.get(checkoutPageElements.summaryDeliverySection).contains('Доставка курьером')
    cy.get(checkoutPageElements.summaryDeliverySection).contains(`${address.street}, ${address.house}`)

    cy.get(checkoutPageElements.backToShippingButton).click()

    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')

    cy.addNewAddress(newAddress)
    cy.addNewReceiver(newReceiver)

    cy.get(checkoutPageElements.moveToPaymentsButton).click()
  
    cy.url().should('include', checkoutPageElements.checkoutPaymentUrl)
    cy.get(checkoutPageElements.summaryDeliverySection).contains('Доставка курьером')
    cy.get(checkoutPageElements.summaryDeliverySection).contains(`${newAddress.street}`)
    
    cy.get(checkoutPageElements.backToShippingButton).click()
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 2)
    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 2)
  })

  it('change shipment method to shop/pvz via UI', () => {
    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')

    cy.get(checkoutPageElements.choosePickupPointButton).click()
    cy.get(checkoutPageElements.pickupAddressField).type(shopAddress, {force: true}).should('have.value', shopAddress)
    cy.get(checkoutPageElements.pickupPoint).should('have.length', 1).click()
    cy.get(checkoutPageElements.confirmPickupPointButton).contains('Выбрать').click()

    cy.get(checkoutPageElements.moveToPaymentsButton).click()

    cy.get(checkoutPageElements.summaryDeliverySection).contains('Забрать из магазина или пункта выдачи заказов')
    cy.get(checkoutPageElements.summaryDeliverySection).contains(shopAddress)

    // cy.get(checkoutPageElements.backToShippingButton).click()

    // cy.get(checkoutPageElements.choosePickupPointButton).click()
    // cy.get(checkoutPageElements.pickupAddressField).type(pvzAddress, {force: true}).should('have.value', pvzAddress)
    // cy.get(checkoutPageElements.pickupPoint).should('have.length', 1).click()
    // cy.get(checkoutPageElements.confirmPickupPointButton).contains('Выбрать').click()

    // cy.get(checkoutPageElements.moveToPaymentsButton).click()
    // cy.get(checkoutPageElements.summaryDeliverySection).contains('Забрать из магазина или пункта выдачи заказов')
    // cy.get(checkoutPageElements.summaryDeliverySection).contains(pvzAddress)
  })

  it('availability of payment methods', () => {
    cy.visit(checkoutPageElements.checkoutShippingUrl)    
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')

    cy.get(checkoutPageElements.choosePickupPointButton).click()
    cy.get(checkoutPageElements.pickupAddressField).type(shopAddress, {force: true}).should('have.value', shopAddress)
    cy.get(checkoutPageElements.pickupPoint).should('have.length', 1).click()
    cy.get(checkoutPageElements.confirmPickupPointButton).contains('Выбрать').click()

    cy.get(checkoutPageElements.moveToPaymentsButton).click()

    cy.get(checkoutPageElements.paymentMethodCard).should('have.length', 3).each((el, i) => {
      cy.wrap(el).should('contain', paymentMethodsTitles[i])
    })

    cy.visit(checkoutPageElements.checkoutShippingUrl) 
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).click()
    cy.get(checkoutPageElements.paymentMethodCard).should('have.length', 3).each((el, i) => {
      cy.wrap(el).should('contain', paymentMethodsTitles[i])
    })
  })

  it('check internal promo', () => {
    const internalPromo = '1231231231231'

    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.showInternalPromo()

    cy.enterInternalCode('1')
    cy.get(checkoutPageElements.internalPromoModalErrorField).should('be.visible').contains('Количество символов должно быть 13')

    cy.enterInternalCode(internalPromo)
    cy.get(checkoutPageElements.internalPromoModal).contains('Код успешно применен')
  })

  // Полезность теста весьма сомнительна
  // it('check payment methods content', () => {
  //   cy.visit(checkoutPageElements.checkoutShippingUrl)
    
  //   cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
  //   cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
  //   cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
  //   cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

  //   cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

  //   cy.get(checkoutPageElements.moveToPaymentsButton).click()
  //   cy.contains('Картой онлайн')
  //   cy.get(checkoutPageElements.payCardButton).should('contain', 'К оформлению')

  //   cy.get(`${checkoutPageElements.paymentMethodCard}:nth-child(2)`).click()
  //   cy.get(checkoutPageElements.payCashButton).should('contain', 'Оплатить')

  //   cy.get(`${checkoutPageElements.paymentMethodCard}:nth-child(3)`).click()
  //   cy.get(checkoutPageElements.creditForm).should('be.visible').contains('В кредит или рассрочку')
  // })

  // отключил, так как 1111 не работает
  // it('check bonus functinality ', () => {
  //   const bonusAmount = 5   
  //   cy.visit(checkoutPageElements.checkoutShippingUrl)    
  //   cy.contains('Адрес доставки')
  //   cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
  //   cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)
  //   cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

  //   cy.get(checkoutPageElements.moveToPaymentsButton).click()
  //   cy.get(commonElements.bonusSection).should('exist')    

  //   cy.get(`${checkoutPageElements.paymentMethodCard}:nth-child(2)`).click()
  //   cy.get(checkoutPageElements.useBonusLink).should('be.visible')

  //   cy.get(`${checkoutPageElements.paymentMethodCard}:nth-child(3)`).click()
  //   cy.get(checkoutPageElements.useBonusLink).should('not.exist')

  //   cy.get(`${checkoutPageElements.paymentMethodCard}:nth-child(1)`).click()
  //   cy.get(checkoutPageElements.useBonusLink).should('be.visible').click()

  //   cy.get(checkoutPageElements.bonusForm).should('be.visible').contains('Бонусы')
  //   cy.get(checkoutPageElements.bonusInputField).should('be.visible').clear().type(bonusAmount)
  //   cy.get(checkoutPageElements.bonusFormButton).click()
  //   cy.contains('Подтвердите списание бонусов')    

  //   cy.enterBonusSms('1234')
    // cy.get(commonElements.notification).should('contain.text', 'Неверный SMS-код')

  //   cy.enterBonusSms('1111')
  //   cy.get(checkoutPageElements.deliveryCostPriceSection).should('be.visible').invoke('text').then((value) => {
  //     expect(parsePriceFromHtml(value)).to.eq(bonusAmount)
  //   })
  // })

  // Отключил, потому что у аккаунта нет бонусов
  // it('check correct bonus calculation', () => {
  //   cy.clearUserCartViaApi()
  //   cy.addToUserCartViaApi(product2.sku)

  //   cy.visit(checkoutPageElements.checkoutShippingUrl)    
  //   cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
  //   cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
  //   cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
  //   cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)
  //   cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

  //   cy.get(checkoutPageElements.moveToPaymentsButton).click()

  //   cy.get(`${checkoutPageElements.paymentMethodCard}:nth-child(1)`).click()
  //   cy.get(checkoutPageElements.useBonusLink).should('be.visible').click()

  //   cy.get(checkoutPageElements.bonusForm).should('be.visible').contains('Бонусы')
  //   cy.get(checkoutPageElements.bonusInputField).should('be.visible').clear().type(product2.cost)
  //   cy.get(checkoutPageElements.bonusFormButton).click()

  //   cy.get(checkoutPageElements.bonusForm).contains(`Сумма не может превышать ${product2.cost - 10} тг.`)  
  // })
})

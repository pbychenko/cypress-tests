import '../../support/productActionsCommands'
import '../../support/accountActionsCommands'
import '../../support/checkoutCommands'
import checkoutPageElements from '../../pageElements/checkoutPageElements'
import { parsePriceFromHtml } from '../../support/utils'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}

const freeDeliveryProduct = { sku:'216442', cost: 274990 }

const receiver = {
  email:"test1@gmail.com",
  firstname: 'tf',
  id: '',
  isDefault: true,
  isOrganization: true,
  lastname: 'tl',
  organization: {
    address: 'add',
    bik: 'bik',
    bin: 'bin',
    iic: 'iic',
    name: 'tf',
  },
  phone: '+7 705 403 92 10',
}

const newReceiver = {
  type: 'jur',
  name: 't2f',
  firstname: 't2f',
  lastName: 't2l',
  phone: '7054039211',
  email: 'test2@gmail.com',
  firstname: 'tf',
  organization: {
    address: 'add2',
    bik: 'bik2',
    bin: 'bin2',
    iic: 'iic2',
    name: 'tf2',
  },
}

const address = {
  apartment: '2',
  city: 'Алматы',
  corpus: '',
  floor: '1',
  geo: {
    lat: "43.203014",
    lng:"76.897872",
  },
  house: '100',
  id: '',
  isDefault: true,
  region: {
    code: '',
    id: 0,
    name: ''
  },
  street: 'проспект Гагарина',
  zipcode: '050000'
}

const getPaymentIntercept = () => {
  cy.intercept({
    method: 'POST',
    url: `${Cypress.env('checkoutApi')}/paymentMethods`,
  }).as('paymentMethods')
}

describe('checkout for jur user', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.requestLogin(existedUser).then(() => {
      cy.clearReceivers()
      cy.addReceiverToAccountViaApi(receiver)

      cy.clearAddresses()
      cy.addAddresToAccountViaApi(address)
      
      cy.clearUserCartViaApi()
      cy.addToUserCartViaApi(freeDeliveryProduct.sku)
    })  
  })

  it('creation new jur receiver on checkout and change type of receiver', () => {
    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.toJurReceiverSwitcher).click()
    cy.get(checkoutPageElements.receiverModalAccept).click()    
    
    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)
    cy.addNewReceiver(newReceiver)
    cy.get(checkoutPageElements.moveToPaymentsButton).click()
    cy.get(checkoutPageElements.backToShippingButton).click()
    cy.get(checkoutPageElements.toJurReceiverSwitcher).click()
    cy.get(checkoutPageElements.receiverModalAccept).click()
    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 2)
  })

  it('check free delivery product', () => {
    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 0)
    
    cy.get(checkoutPageElements.toJurReceiverSwitcher).click()
    cy.get(checkoutPageElements.receiverModalAccept).click()

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.enabled').click()
    
    getPaymentIntercept()
    cy.wait('@paymentMethods').its('response.statusCode').should('eq', 200)

    cy.get(checkoutPageElements.deliveryCostPriceSection).should('be.visible').invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(1000)
    })    

    cy.get(checkoutPageElements.grandTotalCostPriceSection).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(freeDeliveryProduct.cost + 1000)
    })    
  })
})

import '../../support/productActionsCommands'
import '../../support/accountActionsCommands'
import '../../support/checkoutCommands'
import checkoutPageElements from '../../pageElements/checkoutPageElements'
import { parsePriceFromHtml } from '../../support/utils'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}

const freeDeliveryProduct = { sku:'216442', cost: 274990 }
const paidDeliveryProduct = { sku:'216686', cost: 100 }

const receiver = {
  email:"test1@gmail.com",
  firstname: 'tf',
  id: '',
  isDefault: true,
  isOrganization: false,
  lastname: 'tl',
  organization: {
    address: '',
    bik: '',
    bin: '',
    iic: '',
    name: '',
  },
  phone: '+7 705 403 92 10',
}

const address = {
  apartment: '2',
  city: 'Алматы',
  corpus: '',
  floor: '1',
  geo: {
    lat: "43.203014",
    lng:"76.897872",
  },
  house: '100',
  id: '',
  isDefault: true,
  region: {
    code: '',
    id: 0,
    name: ''
  },
  street: 'проспект Гагарина',
  zipcode: '050000'
}

const getPaymentIntercept = () => {
  cy.intercept({
    method: 'POST',
    url: `${Cypress.env('checkoutApi')}/paymentMethods`,
  }).as('paymentMethods')
}

describe('delivery cost calculation on checkout', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.requestLogin(existedUser).then(() => {
      cy.clearReceivers()
      cy.addReceiverToAccountViaApi(receiver)

      cy.clearAddresses()
      cy.addAddresToAccountViaApi(address)      
    })  
  })

  it('check free delivery product', () => {
    cy.clearUserCartViaApi()
    cy.addToUserCartViaApi(freeDeliveryProduct.sku)

    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.enabled').click()

    getPaymentIntercept()
    cy.wait('@paymentMethods').its('response.statusCode').should('eq', 200)

    cy.get(checkoutPageElements.deliveryCostSection).should('be.visible').contains('Бесплатно')
    cy.get(checkoutPageElements.grandTotalCostPriceSection).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(freeDeliveryProduct.cost)
    })    
  })

  it('check paid delivery product', () => {
    cy.clearUserCartViaApi()
    cy.addToUserCartViaApi(paidDeliveryProduct.sku)

    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.enabled').click()
    
    getPaymentIntercept()
    cy.wait('@paymentMethods').its('response.statusCode').should('eq', 200)

    cy.get(checkoutPageElements.deliveryCostPriceSection).should('be.visible').invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(1000)
    })    

    cy.get(checkoutPageElements.grandTotalCostPriceSection).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(paidDeliveryProduct.cost + 1000)
    })    
  })
})

import '../../support/productActionsCommands'
import checkoutPageElements from '../../pageElements/checkoutPageElements'
import cartElements from "../../pageElements/cartElements"
import commonElements from "../../pageElements/commonElements"

const newUserNumber = '7054039212'
const existedUserNumber = '7054039210'
const correctPassword = 'Password1'
const existedUserEmail = 'cbpa@technodom.kz'
const sku = '216442'
const correctSms = '1111'
const incorrectSms = '1234'

describe('checkout for unauthorized user', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')

    cy.visit(`/p/${sku}`)
    cy.addToGuestCartViaApi(sku)
  })

  // отключил тест, потому что 1111 на stage не проходит
  // it('fast login on checkout', () => {
  //   cy.get(checkoutPageElements.fastLoginPhoneField).should('be.visible').type(existedUserNumber)
  //   cy.get(checkoutPageElements.fastLoginNextButton).click()
  //   cy.get(checkoutPageElements.fastLoginSmsField).should('be.visible').type(incorrectSms)
  //   cy.contains('Неверный SMS-код')

  //   cy.get(checkoutPageElements.fastLoginSmsField).should('be.visible').clear().type(correctSms)
  //   cy.url().should('include', checkoutPageElements.checkoutShippingUrl)
  //   cy.contains('Курьером')
  // })

  it('simple login on checkout', () => {
    cy.visit(checkoutPageElements.checkoutPhoneVerificationUrl)
    cy.get(checkoutPageElements.fastLoginPhoneField).should('be.visible')
    cy.uiLogin(existedUserNumber, correctPassword)
    cy.url().should('include', checkoutPageElements.checkoutShippingUrl)
    cy.contains('Курьером')
  })

  it('fast registration with new number and incorrect email', () => {
    cy.visit(checkoutPageElements.checkoutPhoneVerificationUrl)
    cy.get(checkoutPageElements.fastLoginPhoneField).should('be.visible').type(newUserNumber)
    cy.get(checkoutPageElements.fastLoginNextButton).click()

    cy.get(checkoutPageElements.fastLoginNameField).should('be.visible').focus().type('name')
    cy.get(checkoutPageElements.fastLoginLastNameField).should('be.visible').type('lastname')
    cy.get(checkoutPageElements.fastLoginEmailField).should('be.visible').type(existedUserEmail)
    cy.get(checkoutPageElements.fastLoginNextButton).click()
    cy.get(commonElements.notification).should('be.visible').contains('Данный e-mail уже используется')
  })
})

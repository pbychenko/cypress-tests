import '../../support/productActionsCommands'
import '../../support/accountActionsCommands'
import '../../support/checkoutCommands'
import checkoutPageElements from '../../pageElements/checkoutPageElements'
import { parsePriceFromHtml } from '../../support/utils'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}

const serviceProduct = { sku:'56657', cost: 15000 }
const simleProduct = { sku:'52171', cost: 164990 }

const receiver = {
  email:"test1@gmail.com",
  firstname: 'tf',
  id: '',
  isDefault: true,
  isOrganization: false,
  lastname: 'tl',
  organization: {
    address: '',
    bik: '',
    bin: '',
    iic: '',
    name: '',
  },
  phone: '+7 705 403 92 10',
}

const address = {
  apartment: '2',
  city: 'Алматы',
  corpus: '',
  floor: '1',
  geo: {
    lat: "43.203014",
    lng:"76.897872",
  },
  house: '100',
  id: '',
  isDefault: true,
  region: {
    code: '',
    id: 0,
    name: ''
  },
  street: 'проспект Гагарина',
  zipcode: '050000'
}

const newAddress = {
  street: 'улица Шевченко, 154',
  floor: 2,
  apartment: 3,
}

const paymentMethodsTitles = ['Картой онлайн', 'Рассрочка / кредит']
const getPaymentIntercept = () => {
  cy.intercept({
    method: 'POST',
    url: `${Cypress.env('checkoutApi')}/paymentMethods`,
  }).as('paymentMethods')
}


describe('checkout for services', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.requestLogin(existedUser).then(() => {
      cy.clearReceivers()
      cy.addReceiverToAccountViaApi(receiver)

      cy.clearAddresses()
      cy.addAddresToAccountViaApi(address)

      cy.clearUserCartViaApi()
      cy.addServiceToUserCartViaApi(serviceProduct.sku)
    })  
  })

  it('creation new address and availability of payment methods for installation', () => {
    cy.clearUserCartViaApi()
    cy.addServiceToUserCartViaApi(serviceProduct.sku)

    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.contains('Установка г. Алматы')
    cy.get(checkoutPageElements.deliveryAddressItemSection).contains(`${address.street}, ${address.house}`)
    cy.get(checkoutPageElements.deliveryAddressItemSection).contains(`Кв. ${address.apartment}, этаж ${address.floor}`)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).contains(`${receiver.firstname} ${receiver.lastname}`)
    cy.get(checkoutPageElements.receiverItemSection).contains(receiver.email)
    cy.get(checkoutPageElements.receiverItemSection).contains(receiver.phone)

    cy.get(checkoutPageElements.moveToPaymentsButton).click()
  
    cy.url().should('include', checkoutPageElements.checkoutPaymentUrl)
    cy.contains('Адрес установки')
    cy.get(checkoutPageElements.summaryDeliverySection).contains(`${address.street}, ${address.house}`)

    cy.get(checkoutPageElements.backToShippingButton).click()
    cy.addNewAddress(newAddress)

    cy.get(checkoutPageElements.moveToPaymentsButton).click()
  
    cy.url().should('include', checkoutPageElements.checkoutPaymentUrl)
    cy.contains('Адрес установки')
    cy.get(checkoutPageElements.summaryDeliverySection).contains(`${newAddress.street}`)
    cy.get(checkoutPageElements.paymentMethodCard).should('have.length', 2).each((el, i) => {
      cy.wrap(el).should('contain', paymentMethodsTitles[i])
    })
  })

  it('installation and simple product', () => {
    cy.addToUserCartViaApi(simleProduct.sku)

    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.get(checkoutPageElements.shippingWayTitle).should('have.class', 'AccentCard_isAccented').contains('Самовывоз')
    cy.get(checkoutPageElements.deliveryWayTitle).contains('Курьером').click()
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.enabled').click()

    cy.contains('Установка г. Алматы')
    cy.contains(`Установить по тому же адресу: ${address.street}, ${address.house}`)

    cy.get(checkoutPageElements.newAddressForInstallation).click()

    cy.get(checkoutPageElements.deliveryAddressItemSection).contains(`${address.street}, ${address.house}`)
    cy.get(checkoutPageElements.deliveryAddressItemSection).contains(`Кв. ${address.apartment}, этаж ${address.floor}`)

    cy.addNewAddress(newAddress)
    cy.get(checkoutPageElements.moveToPaymentsButton).click()

    getPaymentIntercept()
    cy.wait('@paymentMethods').its('response.statusCode').should('eq', 200)

    cy.get(checkoutPageElements.paymentMethodCard).should('have.length', 2).each((el, i) => {
      cy.wrap(el).should('contain', paymentMethodsTitles[i])
    })

    cy.get(checkoutPageElements.summaryDeliverySection).contains('Доставка курьером')
    cy.get(checkoutPageElements.summaryDeliverySection).contains(`${address.street}, ${address.house}`)

    cy.contains('Адрес установки')
    cy.contains(`${newAddress.street}`)

    cy.get(checkoutPageElements.grandTotalCostPriceSection).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(simleProduct.cost + serviceProduct.cost)
    })
  })
})

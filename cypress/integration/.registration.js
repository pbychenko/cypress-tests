import mainPageElements from '../pageElements/mainPageElements'

const { headerMenuAccount } = mainPageElements
const validSmsCode = '1111'
const invalidSmsCode = '1211'
const existedUser = {
  email: 'user1@example.com',
  phone: '+77017777700',
  password: 'my-secret-password',
  firstname: 'Иван',
  lastname: 'Иванов',
  patronymic: 'Петрович',
  lang: 'kz',
}

it('Does not do much!', () => {
  cy.visit('/')
  cy.contains('Акции')
})

// Тесты временно отключены, так как для корректного исполнения, нужен метод для удаления аккаунта из ссо(чтобы чистить за собой)
// describe('registration via ui header', () => {
//   beforeEach(() => {
//     cy.visit('/')
//     localStorage.setItem('verified-city', JSON.stringify({data: "almaty", expiration: 604800, createdAt: Date.now()}))
//     localStorage.setItem('is-verify-city-modal-shown', JSON.stringify({data: true, createdAt: Date.now()}))
//   })

// it('registration with unregistered email and unregistered phone', () => {
//   const { firstname, lastname, email, phone, password } = existedUser
//   cy.uiRegister(firstname, lastname, email, phone, password)
//   cy.contains('Подтвердите номер телефона')
//   cy.contains('Введите код из СМС')
//   cy.smsConfirm(validSmsCode)
//   cy.get(headerMenuAccount).should('exist').should('be.visible')
// })

// it('registration with registered phone and registered email', () => {
//   cy.request('PUT', 'https://magento-dev.technodom.kz:8080/sso/api/v1/auth/signup', existedUser)
//   .then((response) => {
//     expect(response.body).to.have.property('status', 'SignUP successful. Verification Required')
//   })
//   const { firstname, lastname, email, phone, password } = existedUser
//   cy.uiRegister(firstname, lastname, email, phone, password)
//   cy.get('.Notification-Text')
//     .should('be.visible')
//     .contains('Пользователь с таким номером телефона или email уже существует.')
// })

// it('registration with incorrect smsCode', () => {
//   const { firstname, lastname, email, phone, password } = existedUser
//   cy.uiRegister(firstname, lastname, email, phone, password)
//   cy.contains('Подтвердите номер телефона')
//   cy.contains('Введите код из СМС')
//   cy.smsConfirm(invalidSmsCode)
//   cy.get('.SmsVerificationForm-Error')
//     .should('be.visible')
//     .contains('Неверный SMS-код.')
// })
// })

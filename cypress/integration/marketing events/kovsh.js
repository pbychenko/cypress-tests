import '../../support/productActionsCommands'
import '../../support/cartCommands'
import cartElements from '../../pageElements/cartElements'
import { parsePriceFromHtml } from '../../support/utils'

const intervalsTitle = ['От 100 000 тг.', 'От 200 001 тг.', 'От 500 001 тг.', 'От 1 000 001 тг.']
const mainProduct = { sku:'52171', price: 164990 }

describe('kovsh tests', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.visit(`/p/${mainProduct.sku}`)

    cy.addToGuestCartViaApi(mainProduct.sku)
    cy.visit('/cart')
  })

  it('check kovsh elements on cart', () => {
    cy.get(cartElements.kovshSection).should('be.visible').contains('Technodom.kz дарит вам подарки')
    cy.get(cartElements.openKovshGiftsButton).contains('Добавить подарок').click()
    cy.contains('Выберите подарок')
    cy.get(cartElements.kovshGiftCard).should('have.length', 5)
    cy.get(cartElements.kovshGiftPrice).each((el) => {
      cy.wrap(el).invoke('text').then((value) => {
        expect(parsePriceFromHtml(value)).to.eq(10)
      })    
    })
    cy.get(cartElements.showMoreKovshGifts).contains('Показать eще подарки').click()
    cy.get(cartElements.kovshGiftCard).should('have.length.above', 10)

    cy.get(cartElements.kovshDescriprionButton).contains('Подробнее об акции').click()
    cy.get(cartElements.kovshModal).should('be.visible').contains('Доступные товары')
    cy.get(cartElements.kovshModal).contains('Подарки зависят от суммы покупки')
    cy.get(cartElements.kovshModalGiftCard).should('have.length', 9)
    cy.get(cartElements.kovshModalShowMoreButton).contains('Показать еще').click()
    cy.get(cartElements.kovshModalGiftCard).should('have.length.above', 9)

    cy.get(cartElements.kovshModalIntervalLink).should('have.length', 4)
    cy.get(cartElements.kovshModalIntervalLink).each((el, i) => {
      cy.wrap(el).invoke('text').then((value) => {
        expect(value.replace(/\xA0/g,' ')).to.eq(intervalsTitle[i])
      })    
    })
  }) 

  // it('check select/change kovsh gift', () => {
  //   cy.get(cartElements.openKovshGiftsButton).click()

  //   cy.get(`${cartElements.kovshGiftCard} .ProductCardV`).each((el) => {
  //     cy.wrap(el).should('not.have.class', '--cardChecked')   
  //   })

  //   cy.get(`${cartElements.kovshGiftCard}:nth-child(1) ${cartElements.kovshGiftCheckbox}`).click()
  //   cy.get(`${cartElements.kovshGiftCard}:nth-child(1) .ProductCardV`).should('have.class', '--cardChecked')

  //   cy.get(cartElements.addKovshGiftButton).should('be.visible').contains('Выбрать подарки').click()

  //   cy.get(cartElements.kovshSection).should('be.visible').contains('Подарок добавлен')
  //   cy.get(cartElements.cartItem).should('have.length', 2)
  //   cy.get(`${cartElements.cartItem}:nth-child(1) ${cartElements.cartItemPrice}`).invoke('text').then((value) => {
  //     expect(parsePriceFromHtml(value)).to.eq(10)
  //   })

  //   cy.get(cartElements.totalSummarySection).invoke('text').then((value) => {
  //     expect(parsePriceFromHtml(value)).to.eq(mainProduct.price)
  //   })

  //   cy.get(cartElements.openKovshGiftsButton).contains('Выбрать другой подарок')
  // })

  // it('disappering of selected kovsh gift after changing price interval', () => {
  //   cy.get(cartElements.openKovshGiftsButton).click()
  //   cy.get(`${cartElements.kovshGiftCard}:nth-child(1) ${cartElements.kovshGiftCheckbox}`).click()
  //   cy.get(cartElements.addKovshGiftButton).click()

  //   cy.get(cartElements.cartItem).should('have.length', 2)
  //   cy.addToGuestCartViaApi(mainProduct.sku)
  //   cy.reload()

  //   cy.get(cartElements.cartItem).should('have.length', 1)
  // })

   // it('correct set of kovsh gifts for the interval', () => {
  //   cy.get(cartElements.kovshBlockGiftLink).should('have.length', 2)
  //   .each((el, i) => {
  //     cy.wrap(el).should('have.attr', 'href').should('contain', kovshGiftsSku.firstInterval[i].toString())
  //   })
  //   cy.addToGuestCartViaApi('154595')
  //   cy.reload()
  //   cy.get(cartElements.kovshBlockGiftLink).should('have.length', 2)
  //   .each((el, i) => {
  //     cy.wrap(el).should('have.attr', 'href').should('contain', kovshGiftsSku.secondInterval[i].toString())
  //     cy.wrap(el).should('have.attr', 'href').should('not.contain', kovshGiftsSku.firstInterval[i].toString())
  //   })
  // })
})

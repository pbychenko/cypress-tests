import productPageElements from '../../pageElements/productPageElements'
import cartElements from '../../pageElements/cartElements'
import commonElements from '../../pageElements/commonElements'
import '../../support/pdpCommands'
import { parsePriceFromHtml } from '../../support/utils'

const product = { sku: '258749', cost: 229990 }

describe('check gift functionality', function() {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
  })

  it('check gift elements on pdp', () => {
    cy.visit(`/p/${product.sku}`)

    cy.get(productPageElements.mainGiftSection).should('be.visible').contains('Подарок к товару')
    cy.get(productPageElements.moveToGiftsButton).should('contain', 'Выбрать подарок').click()

    cy.get(productPageElements.chooseGiftSection).should('be.visible').scrollIntoView().contains('Technodom.kz дарит вам подарки')
    cy.get(productPageElements.giftCard).should('have.length', 5)
    cy.get(productPageElements.showMoreGifts).contains('Показать eще подарки').click()
    cy.get(productPageElements.giftCard).should('have.length', 7)

    cy.get(productPageElements.showMoreGifts).contains('Свернуть').click()
    cy.get(productPageElements.giftCard).should('have.length', 5)

    cy.get(productPageElements.giftCardPriceSection).each((el, i) => {
      cy.wrap(el).invoke('text').then((value) => {
        expect(parsePriceFromHtml(value)).to.eq(10)
      })    
    })

    cy.get(productPageElements.addToCartButton).click()
    cy.get(commonElements.notification).contains('Выберите подарок')

    cy.get(`${productPageElements.giftCard} .ProductCardV`).each((el) => {
      cy.wrap(el).should('not.have.class', '--cardChecked')   
    })

    cy.get(`${productPageElements.giftCard}:nth-child(1) ${productPageElements.giftCardChooseCheckbox}`).click()
    cy.get(`${productPageElements.giftCard}:nth-child(1) .ProductCardV`).should('have.class', '--cardChecked')
  })

  it('check gift elements on cart', () => {
    cy.visit(`/p/${product.sku}`)
    cy.get(`${productPageElements.giftCard}:nth-child(3) ${productPageElements.giftCardChooseCheckbox}`).click()
    cy.get(productPageElements.addToCartButton).click()
    cy.get(cartElements.addToCartModal).should('be.visible').contains('Вы добавили 2 товара в корзину')

    cy.visit('/cart')

    cy.get(cartElements.giftKitItem).should('be.visible')
    cy.get(cartElements.giftKitItemSticker).should('contain', 'Подарок')
    cy.get(cartElements.giftKitItemPrice).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(10)
    })

    cy.get(cartElements.totalSummarySection).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(product.cost)
    })
  })
})
import productPageElements from '../../pageElements/productPageElements'
import cartElements from '../../pageElements/cartElements'
import '../../support/pdpCommands'
import { parsePriceFromHtml } from '../../support/utils'

const mainProduct = { sku: '230919', price: 219990 }
const kitProducts = [
  {
    sku: '229614',
    price: 1256
  },
  {
    sku: '229277',
    price: 24990
  },
]

describe('check kit functionality', function() {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
  })

  it('check kit elements on pdp', () => {
    cy.visit(`/p/${mainProduct.sku}`)

    cy.get(productPageElements.chooseKitSection).should('be.visible').scrollIntoView().contains('Комплектом дешевле')
    cy.get(productPageElements.nextKitsButton).click({ multiple: true, force: true })
    cy.get(productPageElements.kitCardList).should('not.have.class', '.--opened')
    cy.get(`${productPageElements.kitCard}:not(.--none)`).should('have.length', 5)
    cy.get(productPageElements.showMoreKitsButton).contains('Показать еще 2 товара').click()
    cy.get(productPageElements.kitCardList).should('not.have.class', '.--opened')

    cy.get(productPageElements.showMoreKitsButton).contains('Скрыть').click()
    cy.get(productPageElements.kitCardList).should('not.have.class', '.--opened')

    cy.get(`${productPageElements.kitCard}:not(.--cardChecked)`).should('have.length', 9)
    cy.get(`${productPageElements.kitCard}:nth-child(5) ${productPageElements.giftCardChooseCheckbox}`).click()
    cy.get(`${productPageElements.kitCard}:nth-child(3) ${productPageElements.giftCardChooseCheckbox}`).click()
    cy.get(`${productPageElements.kitCard}:nth-child(4) ${productPageElements.giftCardChooseCheckbox}`).click()
    cy.get(`${productPageElements.kitCard}:not(.--cardChecked)`).should('have.length', 6)
  })

  it('check kit elements on cart', () => {
    const kitSum = kitProducts.reduce((acc, currentEl) => (acc + currentEl.price), 0)
    cy.visit(`/p/${mainProduct.sku}`)    

    cy.get(`${productPageElements.kitCard}:nth-child(2) ${productPageElements.giftCardChooseCheckbox}`).click()
    cy.get(`${productPageElements.kitCard}:nth-child(3) ${productPageElements.giftCardChooseCheckbox}`).click()
    cy.get(productPageElements.kitsSummarySection).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(mainProduct.price + kitSum)
    })

    cy.get(productPageElements.addToCartButton).click()
    cy.get(cartElements.addToCartModal).should('be.visible').contains('Вы добавили 3 товара в корзину')

    cy.visit('/cart')

    cy.get(cartElements.giftKitItem).each((el) => {
        cy.wrap(el).should('be.visible')
    })
    cy.get(cartElements.giftKitItemSticker).each((el) => {
      cy.wrap(el).should('contain', 'В комплекте')
    })
    cy.get(cartElements.totalSummarySection).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(mainProduct.price + kitSum)
    })
  })
})
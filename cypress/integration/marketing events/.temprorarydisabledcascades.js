import '../../support/productActionsCommands'
import '../../support/cartCommands'
import { parsePriceFromHtml } from '../../support/utils'
import cartElements from '../../pageElements/cartElements'
import checkoutPageElements from '../../pageElements/checkoutPageElements'

const existedUser = {
  phone: '+77054039210',
  password: 'Password1',
}

const receiver = {
  email:"test1@gmail.com",
  firstname: 'tf',
  id: '',
  isDefault: true,
  isOrganization: false,
  lastname: 'tl',
  organization: {
    address: '',
    bik: '',
    bin: '',
    iic: '',
    name: '',
  },
  phone: '+7 705 403 92 10',
}

const jurReceiver = {
  email:"test1@gmail.com",
  firstname: 'tf',
  id: '',
  isDefault: true,
  isOrganization: true,
  lastname: 'tl',
  organization: {
    address: 'add',
    bik: 'bik',
    bin: 'bin',
    iic: 'iic',
    name: 'tf',
  },
  phone: '+7 705 403 92 10',
}

const address = {
  apartment: '2',
  city: 'Алматы',
  corpus: '',
  floor: '1',
  geo: {
    lat: "43.203014",
    lng:"76.897872",
  },
  house: '100',
  id: '',
  isDefault: true,
  region: {
    code: '',
    id: 0,
    name: ''
  },
  street: 'проспект Гагарина',
  zipcode: '050000'
}

const product1 = { sku:'216442', cost: 249990 }
const product2 = { sku:'216442', cost: 249990 }
const product3 = { sku:'216442', cost: 249990 }

describe('check cascade functionality', () => {
  beforeEach(() => {
    cy.setCookie('is-verify-city-modal-shown', 'true')
    cy.requestLogin(existedUser).then(() => {
      cy.clearReceivers()
      cy.addReceiverToAccountViaApi(receiver)

      cy.clearAddresses()
      cy.addAddresToAccountViaApi(address)

      cy.clearUserCartViaApi()
      cy.addToUserCartViaApi(product1.sku)
      cy.addToUserCartViaApi(product2.sku)
    })  
  })

  it('check cascade elements on cart and checkout', () => {
    cy.visit('/cart')

    cy.get(cartElements.cascadeTitle).contains('Что такое каскадная скидка?')
    cy.get(cartElements.cascadeSticker).contains('Скидка 20% на один товар')

    cy.get(cartElements.cascadeTerm).should('be.visible').contains('Каскадная скидка')
    cy.get(cartElements.cascadeCost).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(product2.cost*0.2)
    })

    cy.addToUserCartViaApi(product3.sku)
    cy.get(cartElements.cascadeSticker).contains('Скидка 50% на один товар')
    cy.get(cartElements.cascadeCost).invoke('text').then((value) => {
      expect(parsePriceFromHtml(value)).to.eq(product3.cost*0.5)
    })

    cy.get(cartElements.toCheckoutButton).click()
    cy.get(checkoutPageElements.moveToPaymentsButton).click()
    cy.wait(1000)

    cy.contains('Каскадная скидка')
    cy.get(checkoutPageElements.grandTotalCostPriceSection).invoke('text').then((value) => {
      const grandTotal = product1.cost + product2.cost + product3.cost *0.8
      expect(parsePriceFromHtml(value)).to.eq(grandTotal)
    })
  })

  it('check that cascades are not available for legals', () => {
    cy.addReceiverToAccountViaApi(jurReceiver)

    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.contains('Адрес доставки')
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)
    
    cy.get(checkoutPageElements.toJurReceiverSwitcher).click()
    cy.get(checkoutPageElements.receiverModalAccept).click()

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.enabled').click()
    cy.wait(1000) // надо потом придумать как сделать нормально     

    cy.get(checkoutPageElements.grandTotalCostPriceSection).invoke('text').then((value) => {
      const grandTotal = product1.cost + product2.cost
      expect(parsePriceFromHtml(value)).to.eq(grandTotal)
    })
  })

  it('check that bonuses are not available for cascades', () => {
    cy.visit(checkoutPageElements.checkoutShippingUrl)
    cy.contains('Адрес доставки')
    cy.get(checkoutPageElements.deliveryWayTitle).should('have.class', 'AccentCard_isAccented').contains('Курьером')
    cy.get(checkoutPageElements.deliveryAddressItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.enabled').click()
    
    cy.get(checkoutPageElements.toJurReceiverSwitcher).click()
    cy.get(checkoutPageElements.receiverModalAccept).click()

    cy.get(checkoutPageElements.receiverItemSection).should('have.length', 1)

    cy.get(checkoutPageElements.moveToPaymentsButton).should('be.enabled').click()
    cy.wait(1000) // надо потом придумать как сделать нормально     

    cy.get(`${checkoutPageElements.paymentMethodCard}:nth-child(1)`).click()
    cy.get(checkoutPageElements.useBonusLink).should('not.be.visible')
  })


  // it('exception good affects the discount', () => {
  //   cy.addToUserCartViaApi('147989')
  //   cy.addToUserCartViaApi('161836')
  //   cy.addToUserCartViaApi('168358')
  //   cy.goToCart()
  //   cy.get(cascadeElements.discountTitle).contains('Cкидка -50% на один товар')
  //   cy.get(cascadeElements.discoundAmount).contains('24 945 ₸')
  // })


  // it('exception good does not affect the discount', () => {
  //   cy.addToUserCartViaApi('51558')
  //   cy.addToUserCartViaApi('145783')
  //   cy.addToUserCartViaApi('151856')
  //   cy.goToCart()
  //   cy.get(cascadeElements.discountTitle).contains('Cкидка -20% на один товар')
  //   cy.get(cascadeElements.discountTitleNot)
  //   cy.get(cascadeElements.discoundAmount).contains('13 978 ₸')
  //   cy.get(cascadeElements.priceCart).contains('275 892 ₸')
  // })

  // it('exception good by price', () => {
  //   cy.addToUserCartViaApi('51558')
  //   cy.addToUserCartViaApi('145783')
  //   cy.addToUserCartViaApi('147988')
  //   cy.goToCart()
  //   cy.get(cascadeElements.discountTitle).contains('Cкидка -20% на один товар')
  //   cy.get(cascadeElements.discountTitleNot)
  //   cy.get(cascadeElements.discoundAmount).contains('13 978 ₸')
  //   cy.get(cascadeElements.priceCart).contains('216 865 ₸')
  // })

  // it('discount changes in cart', () => {
  //   cy.addToUserCartViaApi('51558')
  //   cy.addToUserCartViaApi('145783')
  //   cy.goToCart()
  //   cy.get(cascadeElements.discountTitle).contains('Cкидка -20% на один товар')
  //   cy.get(cascadeElements.increase).click()
  //   cy.get(cascadeElements.discountTitle).contains('Cкидка -50% на один товар')

  // })

  // it('100% discount', () => {
  //   cy.addToUserCartViaApi('147989')
  //   cy.addToUserCartViaApi('147989')
  //   cy.addToUserCartViaApi('147989')
  //   cy.addToUserCartViaApi('161836')
  //   cy.addToUserCartViaApi('145783')
  //   cy.goToCart()
  //   cy.get(cascadeElements.discountTitle).contains('Один товар в подарок')
  //   cy.get(cascadeElements.discoundAmount).contains('49 880 ₸')
  // })
})
